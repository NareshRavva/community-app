package com.verbosetech.weshare.application;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.vanniktech.emoji.EmojiManager;
import com.vanniktech.emoji.google.GoogleEmojiProvider;
import com.verbosetech.weshare.util.Prefs;

import io.branch.referral.Branch;
import io.fabric.sdk.android.Fabric;

/**
 * Created by mayank on 4/7/16.
 */
public class BaseApplication extends Application {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        Branch.getAutoInstance(this);
        EmojiManager.install(new GoogleEmojiProvider());
        MobileAds.initialize(this, "ca-app-pub-2538987126833754~6367989732");
        FirebaseApp.initializeApp(this);
        Prefs.initPrefs(this);

        FireBaseSettingsInit();
    }

    private void FireBaseSettingsInit() {
        FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        firestore.setFirestoreSettings(settings);
    }
}
