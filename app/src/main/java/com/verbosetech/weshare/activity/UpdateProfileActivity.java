package com.verbosetech.weshare.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;
import com.verbosetech.weshare.R;
import com.verbosetech.weshare.communityList.CommunityListModel;
import com.verbosetech.weshare.firestore.FireStoreConstants;
import com.verbosetech.weshare.firestore.FireStoreOperations;
import com.verbosetech.weshare.model.UserData;
import com.verbosetech.weshare.util.Constants;
import com.verbosetech.weshare.util.Helper;
import com.verbosetech.weshare.util.Prefs;
import com.verbosetech.weshare.view.CircularImageView;
import com.verbosetech.weshare.view.MontserratMediumTextView;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * The type Sign up activity.
 */
public class UpdateProfileActivity extends AppCompatActivity {
    @BindView(R.id.img_profile)
    CircularImageView imgProfile;
    @BindView(R.id.input_layout_user_name)
    TextInputLayout inputLayoutUserName;
    @BindView(R.id.et_user_name)
    EditText etUserName;
    @BindView(R.id.input_layout_email)
    TextInputLayout inputLayoutEmail;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.input_layout_mobile_number)
    TextInputLayout inputLayoutMobileNumber;
    @BindView(R.id.et_mobile_number)
    EditText etMobileNumber;
    @BindView(R.id.submit_imv)
    ImageView submit_imv;
    @BindView(R.id.imv_upload)
    ImageView imv_upload;

    @BindView(R.id.tv_verified_email)
    MontserratMediumTextView tvVerifiedEmail;

    @BindView(R.id.tv_verified_mblno)
    MontserratMediumTextView tvVerifiedMblno;


    private static final int GALLERY_PICK = 1;
    private UserData userData;
    private String currentUser, mobileNumber;
    // Storage Firebase
    private StorageReference mImageStorage;
    private String TAG = "ProfileActivity";
    ProgressDialog progressDialog;
    private CommunityListModel communityModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
        ButterKnife.bind(this);

        final String commModel = getIntent().getStringExtra(Constants.COMMUNITY_DATA);
        getData(commModel);

        mImageStorage = FirebaseStorage.getInstance().getReference();
        getUserDetails();

    }

    private void getData(String commModel) {
        Gson gson = new Gson();
        communityModel = gson.fromJson(commModel, CommunityListModel.class);
    }


    private void getUserDetails() {

        if (communityModel != null) {
            etMobileNumber.setText(Prefs.getString(Prefs.PHONE_NUMBER, ""));
            tvVerifiedMblno.setVisibility(View.VISIBLE);
            FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
            FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();

            if (firebaseUser != null) {
                currentUser = FireStoreOperations.getInstance().getUserId();
                if (!TextUtils.isEmpty(currentUser)) {
                    showProgress("UserData", "Please wait...");

                    DocumentReference dr = FirebaseFirestore.getInstance().
                            collection(FireStoreConstants.TABLE_USERS).
                            document(communityModel.getId()).
                            collection(FireStoreConstants.USERS_DATA)
                            .document(currentUser);
                    dr.get().
                            addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                @Override
                                public void onSuccess(DocumentSnapshot documentSnapshot) {
                                    if (documentSnapshot.exists()) {
                                        userData = documentSnapshot.toObject(UserData.class);
                                        setUserProfileData(userData);
                                        hideProgress();
                                    } else {
                                        hideProgress();
                                    }
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            hideProgress();
                        }
                    });

                }


            }
        }
    }

    private void setUserProfileData(final UserData userData) {
        if (!TextUtils.isEmpty(userData.getName())) {
            etUserName.setText(userData.getName());
        }
        if (!TextUtils.isEmpty(userData.getEmail())) {
            etEmail.setText(userData.getEmail());
        } else {
            etEmail.setEnabled(true);
            etEmail.setClickable(true);
        }
        if (!TextUtils.isEmpty(userData.getMobile_no())) {
            etMobileNumber.setText(userData.getMobile_no());
            mobileNumber = userData.getMobile_no();
        } else {
            setSIMNumber();
        }
        if (!TextUtils.isEmpty(userData.getProfile_pic_tumbnail())) {
           /* Picasso.with(ProfileActivity.this).load(userData.getProfile_pic_tumbnail()).networkPolicy(NetworkPolicy.OFFLINE)
                    .placeholder(R.drawable.avatar).into(imgProfile, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {

                    Picasso.with(ProfileActivity.this)
                            .load(userData.getProfile_pic_tumbnail())
                            .placeholder(R.drawable.avatar)
                            .into(imgProfile);

                }
            });*/


        }


    }

    private void setSIMNumber() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            List<SubscriptionInfo> subscription = SubscriptionManager.from(getApplicationContext()).getActiveSubscriptionInfoList();
            for (int i = 0; i < subscription.size(); i++) {
                SubscriptionInfo info = subscription.get(i);
                Log.d(TAG, "number " + info.getNumber());
                if (!TextUtils.isEmpty(info.getNumber())) {
                    etMobileNumber.setText(info.getNumber());
                    break;
                }
            }
        }
    }


    /**
     * Update data.
     */
    @OnClick(R.id.submit_imv)
    void updateData() {
        if (validation()) {
            sendDataToServer();
        }
    }

    /**
     * Validation boolean.
     *
     * @return the boolean
     */
    boolean validation() {
        if (TextUtils.isEmpty(etUserName.getText().toString())) {
            inputLayoutUserName.setError(getString(R.string.please_enter_your_name));
            return false;
        } else {
            inputLayoutUserName.setErrorEnabled(false);
        }
        if (TextUtils.isEmpty(etEmail.getText().toString())) {
            inputLayoutEmail.setError(getString(R.string.please_enter_your_email));
            return false;
        } else {
            inputLayoutEmail.setErrorEnabled(false);
        }
        if (!Helper.validate(etEmail.getText().toString())) {
            inputLayoutEmail.setError(getString(R.string.please_enter_valid_email));
            return false;
        } else {
            inputLayoutEmail.setErrorEnabled(false);
        }
        if (!TextUtils.isEmpty(etEmail.getText().toString()) && !Helper.validate(etEmail.getText().toString())) {
            inputLayoutEmail.setError(getString(R.string.please_enter_valid_email));
            return false;
        } else {
            inputLayoutEmail.setErrorEnabled(false);
        }
        if (TextUtils.isEmpty(etMobileNumber.getText().toString())) {
            inputLayoutMobileNumber.setError(getString(R.string.please_enter_your_mobile_number));
            return false;
        } else {
            inputLayoutMobileNumber.setErrorEnabled(false);
        }

        return true;
    }

    private void sendDataToServer() {
        showProgress("Updating..", "Please wait...");
        if (!TextUtils.isEmpty(currentUser)) {
            if (userData == null) {
                userData = new UserData();
            }
            String device_token = FirebaseInstanceId.getInstance().getToken();

            userData.setName(etUserName.getText().toString());
            userData.setEmail(etEmail.getText().toString());
            userData.setMobile_no(etMobileNumber.getText().toString());
            userData.setProfile_pic("default");
            userData.setProfile_pic_tumbnail("default");
            userData.setOs("ANDROID");
            userData.setOnline_status("true");
            userData.setDevice_token(device_token);

            FirebaseFirestore.getInstance().
                    collection(FireStoreConstants.TABLE_USERS).
                    document(communityModel.getId()).
                    collection(FireStoreConstants.USERS_DATA).
                    document(currentUser)
                    .set(userData, SetOptions.merge()).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    dashBoard();
                    Prefs.putString(Prefs.USER_NAME, etUserName.getText().toString());
                    Prefs.putString(Prefs.USER_EMIL, etEmail.getText().toString());
                    Prefs.putString(Prefs.USER_PHONE, etMobileNumber.getText().toString());
                    hideProgress();

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    hideProgress();
                    //getUserDetails();
                }
            });
        }
    }


    /**
     * Image slections and upload.
     */
    @OnClick(R.id.imv_upload)
    void imageSlectionsAndUpload() {
        startCropImageActivity(null);
        /*if (isStoragePermissionGranted()) {
            startCropImageActivity(null);
        }*/
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        /*CropImage.activity(imageUri).setGuidelines(CropImageView.Guidelines.ON).setMultiTouchEnabled(true).setInitialCropWindowPaddingRatio(0).setAspectRatio(1, 1)
                .start(this);*/
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

     /*   if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {


                showProgress("Uploading Image...", "Please wait while we upload and process the image.");
                Uri resultUri = result.getUri();

                File thumb_filePath = new File(resultUri.getPath());

                if (!TextUtils.isEmpty(currentUser)) {

                    setProfileImage(thumb_filePath, resultUri);


                }


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                Exception error = result.getError();

            }
        }*/


    }

    private void setProfileImage(File thumb_filePath, Uri resultUri) {
      /*  if (thumb_filePath.exists() && resultUri != null) {
            final Bitmap thumb_bitmap = new Compressor(this)
                    .setMaxWidth(200)
                    .setMaxHeight(200)
                    .setQuality(75)
                    .compressToBitmap(thumb_filePath);


            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            thumb_bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            final byte[] thumb_byte = baos.toByteArray();


            StorageReference filepath = mImageStorage.child("profile_images").child(currentUser + ".jpg");
            final StorageReference thumb_filepath = mImageStorage.child("profile_images").child("thumbs").child(currentUser + ".jpg");

            filepath.putFile(resultUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                    if (task.isSuccessful()) {

                        final String download_url = task.getResult().getDownloadUrl().toString();

                        UploadTask uploadTask = thumb_filepath.putBytes(thumb_byte);
                        uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> thumb_task) {

                                final String thumb_downloadUrl = thumb_task.getResult().getDownloadUrl().toString();

                                if (thumb_task.isSuccessful()) {

                                    HashMap<String, Object> update_hashMap = new HashMap<String, Object>();
                                    update_hashMap.put("profile_pic", download_url);
                                    update_hashMap.put("profile_pic_tumbnail", thumb_downloadUrl);


                                    FirebaseFirestore.getInstance()
                                            .collection(Constants.FB_TABLE_USERS)
                                            .document(currentUser)
                                            .update(update_hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            hideProgress();
                                            userData.setProfile_pic(download_url);
                                            userData.setProfile_pic_tumbnail(thumb_downloadUrl);
                                            imgProfile.setImageBitmap(thumb_bitmap);
                                        }
                                    });


                                } else {

                                    Toast.makeText(ProfileActivity.this, "Error in uploading thumbnail.", Toast.LENGTH_LONG).show();
                                    hideProgress();

                                }


                            }
                        });


                    } else {

                        Toast.makeText(ProfileActivity.this, "Error in uploading.", Toast.LENGTH_LONG).show();
                        hideProgress();

                    }

                }
            });
        } else {
            hideProgress();
        }*/
    }

    /**
     * Back arrow.
     */
    @OnClick(R.id.ivBack)
    void backArrow() {

        dashBoard();

    }

    @Override
    public void onBackPressed() {
        if (validation()) {
            dashBoard();
        }
        super.onBackPressed();
    }


    private void dashBoard() {
        Intent mainIntent = new Intent(UpdateProfileActivity.this, MainActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
      /*  if (!TextUtils.isEmpty(mobileNumber)
                && !TextUtils.isEmpty(etMobileNumber.getText().toString())
                && !mobileNumber.equalsIgnoreCase(etMobileNumber.getText().toString())) {
            Prefs.putBoolean(Prefs.OTP_VERIFIED, false);
        }

        if (!TextUtils.isEmpty(etMobileNumber.getText().toString())) {
            if (!Prefs.getBoolean(Prefs.OTP_VERIFIED, false)) {
                startOtpScreen();
            } else {
                startMainScreen();
            }
        } else {
            startMainScreen();
        }
*/
    }

  /*  void startOtpScreen() {
        Intent mainIntent = new Intent(ProfileActivity.this, OtpVerifyActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        mainIntent.putExtra(Constants.PHONE, etMobileNumber.getText().toString());
        Prefs.putString(Prefs.PHONE_NO, etMobileNumber.getText().toString().trim());
        startActivity(mainIntent);
    }

    void startMainScreen() {
        Intent mainIntent = new Intent(ProfileActivity.this, GroupsListActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
    }*/

    protected void showProgress(String title, String message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
        }
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    protected void hideProgress() {
        progressDialog.hide();
    }

}

