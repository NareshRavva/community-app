package com.verbosetech.weshare.firestore;

public class FireStoreConstants {

    public static final String TABLE_USERS = "table_users";
    public static final String TABLE_COMMUNITY_LIST = "table_community_list";
    public static final String TABLE_COMMUNITY_USER = "table_community_users";
    public static final String USERS_DATA = "users_data";
    public static final String USERS_FEEDS = "user_feeds";
    public static final String USER_CHAT_HISTORY = "user_chat_hisitory";
    public static final String MESSAGES = "Messages";

    public static final String SMAPLE = "community_ramky_towers";
    public static final String SAMPLE_KEY = "w1Cd1oVnBxgg8kFKBXogE8N8VzA2";
    public static final String SAMPLE_KEY_1= "3LvKUEacj4ScislIJeYUY7sSEfS2";
    public static final String COMMUNITY_ID = "Community_id";
    public static final String TABLE_POST_COMMENTS= "table_post_comments";
    public static final String COMMENTS="comments";
}
