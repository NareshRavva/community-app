package com.verbosetech.weshare.firestore;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/*
 * Created by developer on 26/12/18.
 */
public class FireStoreOperations {
    private static final FireStoreOperations ourInstance = new FireStoreOperations();
    public static FireStoreOperations getInstance() {
        return ourInstance;
    }
    public String getUserId() {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        if (firebaseUser != null) {
            return firebaseUser.getUid();
        }


        return "";
    }
}
