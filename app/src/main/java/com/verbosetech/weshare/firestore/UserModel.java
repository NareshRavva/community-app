package com.verbosetech.weshare.firestore;

import java.util.ArrayList;

public class UserModel {

    String id;
    String name;
    String gender;
    ArrayList<String> social_profiles_urls = new ArrayList<>();
    String primary_email;
    boolean primary_email_verified;
    String role;
    String primary_mobile;
    boolean primary_mobile_isverified;
    String status;
    String location;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public ArrayList<String> getSocial_profiles_urls() {
        return social_profiles_urls;
    }

    public void setSocial_profiles_urls(ArrayList<String> social_profiles_urls) {
        this.social_profiles_urls = social_profiles_urls;
    }

    public String getPrimary_email() {
        return primary_email;
    }

    public void setPrimary_email(String primary_email) {
        this.primary_email = primary_email;
    }

    public boolean isPrimary_email_verified() {
        return primary_email_verified;
    }

    public void setPrimary_email_verified(boolean primary_email_verified) {
        this.primary_email_verified = primary_email_verified;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPrimary_mobile() {
        return primary_mobile;
    }

    public void setPrimary_mobile(String primary_mobile) {
        this.primary_mobile = primary_mobile;
    }

    public boolean getPrimary_mobile_isverified() {
        return primary_mobile_isverified;
    }

    public void setPrimary_mobile_isverified(boolean primary_mobile_isverified) {
        this.primary_mobile_isverified = primary_mobile_isverified;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
