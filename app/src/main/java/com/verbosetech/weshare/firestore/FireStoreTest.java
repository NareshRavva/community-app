package com.verbosetech.weshare.firestore;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.verbosetech.weshare.R;
import com.verbosetech.weshare.communityList.CommunityListModel;

import java.util.ArrayList;
import java.util.List;

public class FireStoreTest extends AppCompatActivity {

    Button btn_add;

    private static final String NAME_KEY = "Name";
    private static final String EMAIL_KEY = "Email";
    private static final String PHONE_KEY = "Phone";


    FirebaseFirestore db;

    private String TAG = this.getClass().getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firestore);

        db = FirebaseFirestore.getInstance();

        btn_add = (Button) findViewById(R.id.btn_add);

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //addNewContact();

                //getCommunityList();

                //getParticularCommunityData();

                updateListeners();
            }
        });
    }

    private void updateListeners() {

        FirebaseFirestore.getInstance().collection(FireStoreConstants.TABLE_USERS).document("community_ramky_towers")
                .collection("feeds").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {

                if (e != null) {
                    Log.i(TAG, "listen:error" + e);
                    return;
                }

                for (DocumentChange dc : queryDocumentSnapshots.getDocumentChanges()) {
                    switch (dc.getType()) {
                        case ADDED:
                            Log.i(TAG, "New city: " + dc.getDocument().getData());
                            break;
                        case MODIFIED:
                            Log.i(TAG, "Modified city: " + dc.getDocument().getData());
                            break;
                        case REMOVED:
                            Log.i(TAG, "Removed city: " + dc.getDocument().getData());
                            break;
                    }

                }
            }
        });

    }

    private void getParticularCommunityData() {
        FirebaseFirestore.getInstance().collection("table_community_list").document("community_ramky_towers")
                .get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                CommunityListModel model = documentSnapshot.toObject(CommunityListModel.class);

                Log.v(TAG, "particular data-->" + model.getDisplay_name());

                Log.v(TAG, "ChatLocation data-->" + model.getLocation());
            }
        });
    }

    private void getCommunityList() {
        FirebaseFirestore.getInstance().collection("table_community_list")
                .get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                List<CommunityListModel> types = queryDocumentSnapshots.toObjects(CommunityListModel.class);

                Log.v(TAG, "types data-->" + types);

            }
        });
    }

    private void addNewContact() {

        UserModel user_model = new UserModel();
        user_model.setId("F6ElmQFhr0coCaUzie1cd5TO4g22");
        user_model.setName("Test User 1");
        user_model.setGender("M");

        ArrayList<String> social_profiles_urls = new ArrayList<>();
        social_profiles_urls.add("Url 1");
        social_profiles_urls.add("Url 2");

        user_model.setPrimary_email("Test User 1");
        user_model.setPrimary_email_verified(true);
        user_model.setName("Test User 1");
        user_model.setRole("Admin");
        user_model.setPrimary_mobile("970303323");
        user_model.setPrimary_mobile_isverified(true);
        user_model.setStatus("online");
        user_model.setLocation("Gachibowli");


        FirebaseFirestore.getInstance().collection(FireStoreConstants.TABLE_USERS).document("community_aparna_silver_oaks").
                collection(FireStoreConstants.USERS_DATA).document(user_model.getId()).set(user_model)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(FireStoreTest.this, "User Data Added", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(Exception e) {
                        Toast.makeText(FireStoreTest.this, "ERROR" + e.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
