package com.verbosetech.weshare.chat.sendlocation.network;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/*
 * Created by developer on 28/9/18.
 */
public class LocationRetrofitClient {

    private static String GOOGLE_URL = "https://maps.googleapis.com";
    static LocationService locationService;
    static Retrofit retrofit;
    public static LocationService create() {
        if (locationService == null)
            locationService = getClient().create(LocationService.class);
        return locationService;
    }

    public static Retrofit getClient() {
        if (retrofit == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).addInterceptor(interceptor).build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(GOOGLE_URL)
                    .client(client)

                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }
        return retrofit;
    }

}
