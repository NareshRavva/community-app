package com.verbosetech.weshare.chat.sendlocation.adapter;

import android.content.Context;
import android.location.Address;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.verbosetech.weshare.R;
import com.verbosetech.weshare.chat.sendlocation.listners.PlaceInfoListner;
import com.verbosetech.weshare.chat.sendlocation.models.PlaceModel;
import com.verbosetech.weshare.chat.sendlocation.utils.LocationUtils;
import com.verbosetech.weshare.util.Logger;
import com.verbosetech.weshare.view.MontserratMediumTextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SearchPlaceAdapter extends
        RecyclerView.Adapter<SearchPlaceAdapter.TextViewHolder> {
    private ArrayList<PlaceModel> resultList;
    private Context context;
    private PlaceInfoListner placeInfoListner;

    public SearchPlaceAdapter(Context context,
                              ArrayList<PlaceModel> resultList,
                              PlaceInfoListner placeInfoListner) {

        this.context = context;
        this.resultList = resultList;
        this.placeInfoListner = placeInfoListner;
    }


    @NonNull
    @Override
    public TextViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new TextViewHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.place_item, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull TextViewHolder holder, int position) {

        final PlaceModel placeModel = resultList.get(position);
        if (placeModel != null && !TextUtils.isEmpty(placeModel.getDescription())) {
            holder.textView.setText(placeModel.getDescription());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LocationUtils.getPlaceInfoByPlaceId(context, placeModel.getPlaceid(), new PlaceInfoListner() {
                        @Override
                        public void getPlaceInfo(Address address) {
                            address.setAddressLine(0, placeModel.getDescription());
                            placeInfoListner.getPlaceInfo(address);
                            Logger.getInstance().d("AddRessInfo-->" + address);
                        }
                    });
                }
            });
        } else {
            return;
        }
    }


    @Override
    public int getItemCount() {
        return resultList.size();
    }


    class TextViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_place)
        MontserratMediumTextView textView;

        TextViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}