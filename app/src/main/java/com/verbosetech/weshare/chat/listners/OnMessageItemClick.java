package com.verbosetech.weshare.chat.listners;

import com.verbosetech.weshare.chat.models.ChatMessage;
import com.verbosetech.weshare.model.Message;

public interface OnMessageItemClick {
    void OnMessageClick(ChatMessage message, int position);

    void OnMessageLongClick(ChatMessage message, int position);
}
