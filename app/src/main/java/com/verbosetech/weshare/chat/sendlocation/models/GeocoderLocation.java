package com.verbosetech.weshare.chat.sendlocation.models;

import lombok.Data;

@Data
public class GeocoderLocation {

    public String locality, district, state, Country, postalCode, fullAdress;
    public double lat, lng;
}
