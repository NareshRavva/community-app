package com.verbosetech.weshare.chat.sendlocation.models;

import lombok.Data;

/*
 * Created by developer on 5/1/19.
 */
@Data
public class NearByPlace {
    String id, place_id,
            placeName = null,
            reference,
            icon,
                vicinity = null;
    double latitude, longitude;

}
