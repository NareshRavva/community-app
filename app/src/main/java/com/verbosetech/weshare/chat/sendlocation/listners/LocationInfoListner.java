package com.verbosetech.weshare.chat.sendlocation.listners;

import com.verbosetech.weshare.chat.sendlocation.models.GeocoderLocation;

/*
 * Created by developer on 4/1/19.
 */
public interface LocationInfoListner {

    void requestsLocationInfo(GeocoderLocation location);

}
