package com.verbosetech.weshare.chat.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.vanniktech.emoji.EmojiTextView;
import com.verbosetech.weshare.R;
import com.verbosetech.weshare.chat.listners.OnMessageItemClick;
import com.verbosetech.weshare.chat.models.AttachmentTypes;
import com.verbosetech.weshare.chat.models.ChatHistoryModel;
import com.verbosetech.weshare.chat.models.ChatMessage;
import com.verbosetech.weshare.firestore.FireStoreOperations;
import com.verbosetech.weshare.model.AttachmentTypes1;
import com.verbosetech.weshare.model.DownloadFileEvent;
import com.verbosetech.weshare.model.Message;
import com.verbosetech.weshare.util.Constants;
import com.verbosetech.weshare.util.FileUtils;
import com.verbosetech.weshare.util.Helper;
import com.verbosetech.weshare.util.LinkTransformationMethod;
import com.verbosetech.weshare.util.MyFileProvider;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
 * Created by developer on 26/12/18.
 */
public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ChatHistoryModel consersation;
    private Integer myId, _48dpInPx;
    private OnMessageItemClick onMessageItemClick;

    public ChatAdapter(Context context, ChatHistoryModel consersation,
                       OnMessageItemClick onMessageItemClick) {
        this.context = context;
        this.consersation = consersation;
        this._48dpInPx = Helper.dp2px(context, 48);
        this.onMessageItemClick = onMessageItemClick;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        switch (viewType) {
           /* case AttachmentTypes.RECORDING:
                return new RecordingMessageViewHolder(LayoutInflater.from(context).inflate(R.layout.item_message_attachment_recording, parent, false));*/
            case AttachmentTypes.IMAGE:
                return new ImageMessageViewHolder(LayoutInflater.from(context).
                        inflate(R.layout.item_message_attachment_image, parent, false));
            case AttachmentTypes.VIDEO:
                return new VideoMessageViewHolder(LayoutInflater.from(context).
                        inflate(R.layout.item_message_attachment_video, parent, false));
            case AttachmentTypes.LOCATION:
                return new LocationMessageViewHolder(LayoutInflater.from(context).
                        inflate(R.layout.item_message_location, parent, false));
            case AttachmentTypes.CONTACT:
                return new ContactMessageViewHolder(LayoutInflater.from(context).
                        inflate(R.layout.item_message_attachment_contact, parent, false));


          /*    case AttachmentTypes.NONE_TYPING:
                return new TypingMessageViewHolder(LayoutInflater.from(context).inflate(R.layout.item_message_typing, parent, false));
            case AttachmentTypes.NONE_TEXT:*/

            case AttachmentTypes.LOADING:
                return new LoadingViewHolder(LayoutInflater.from(context).inflate(R.layout.item_message_loading, parent, false));

            default:
                return new TextMessageViewHolder(LayoutInflater.from(context).
                        inflate(R.layout.item_message_text, parent, false));
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ImageMessageViewHolder) {
            ImageMessageViewHolder imageMessageViewHolder = (ImageMessageViewHolder) holder;
            imageMessageViewHolder.setData(consersation.getListMessageData().get(position), context);
        } else if (holder instanceof VideoMessageViewHolder) {
            VideoMessageViewHolder videoMessageViewHolder = (VideoMessageViewHolder) holder;
            videoMessageViewHolder.setData(consersation.getListMessageData().get(position), context);
        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.setData(consersation.getListMessageData().get(position), context);
        } else if (holder instanceof LocationMessageViewHolder) {
            LocationMessageViewHolder locationMessageViewHolder = (LocationMessageViewHolder) holder;
            locationMessageViewHolder.setData(consersation.getListMessageData().get(position), context);
        } else if (holder instanceof ContactMessageViewHolder) {
            ContactMessageViewHolder contactMessageViewHolder = (ContactMessageViewHolder) holder;
            contactMessageViewHolder.setData(consersation.getListMessageData().get(position), context);
        } else {
            TextMessageViewHolder textMessageViewHolder = (TextMessageViewHolder) holder;
            textMessageViewHolder.setData(consersation.getListMessageData().get(position), context);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return consersation.getListMessageData().
                get(position).getAttachmentType();
    }

    @Override
    public int getItemCount() {
        return consersation.getListMessageData().size();
    }


    class TextMessageViewHolders extends BaseMessageViewHolder {


        @BindView(R.id.text)
        EmojiTextView text;


        TextMessageViewHolders(View itemView) {
            super(itemView);
            text.setTransformationMethod(new LinkTransformationMethod());
            text.setMovementMethod(LinkMovementMethod.getInstance());


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    if (pos != -1) {
                        onMessageItemClick.OnMessageClick(consersation.getListMessageData().get(pos), pos);
                    }

                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    int pos = getAdapterPosition();
                    if (pos != -1) {
                        onMessageItemClick.OnMessageLongClick(consersation.getListMessageData().get(pos), pos);
                    }
                    return false;
                }
            });

        }

        @Override
        protected void setData(ChatMessage message) {
            super.setData(message);
            text.setText(message.getBody());
        }
    }

    class BaseMessageViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.senderName)
        TextView senderName;
        @BindView(R.id.card_view)
        CardView cardView;
        @BindView(R.id.container)
        LinearLayout ll;
        private boolean isMine, done;

        BaseMessageViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        protected void setData(ChatMessage message) {
            this.isMine = FireStoreOperations.getInstance().getUserId().equals(message.getSenderId());
            if (!message.isLoading()) {
                if (message.getAttachmentType() == AttachmentTypes.NONE_TYPING)
                    return;

                senderName.setVisibility(View.GONE);
                //senderName.setText(isMine() ? "You" : message.getSenderName());
                time.setText(Helper.getTime(message.getDate()));
                time.setCompoundDrawablesWithIntrinsicBounds(0, 0, isMine() ? (message.isSent() ? (message.isDelivered() ? R.drawable.ic_done_all_black : R.drawable.ic_done_black) : R.drawable.ic_waiting) : 0, 0);
                cardView.setCardBackgroundColor(ContextCompat.getColor(context, message.isSelected() ? R.color.colorDivider : R.color.colorBgLight));
                ll.setBackgroundColor(message.isSelected() ? ContextCompat.getColor(context, R.color.colorDivider) : isMine() ? Color.WHITE : ContextCompat.getColor(context, R.color.colorBgLight));
            }
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) cardView.getLayoutParams();
            if (isMine()) {
                layoutParams.gravity = Gravity.END;
                layoutParams.setMargins(_48dpInPx, 0, 0, 0);
            } else {
                layoutParams.gravity = Gravity.START;
                layoutParams.setMargins(0, 0, _48dpInPx, 0);
            }
            cardView.setLayoutParams(layoutParams);
        }

        boolean isMine() {
            return this.isMine;
        }

        /* *//* void onItemClick(boolean b, int pos) {
             if (itemClickListener != null) {
                 if (b)
                     itemClickListener.OnMessageClick(dataList.get(pos), pos);
                 else
                     itemClickListener.OnMessageLongClick(dataList.get(pos), pos);
             }
         }
 */
        void broadcastDownloadEvent(Message message) {
            Intent intent = new Intent(Constants.BROADCAST_DOWNLOAD_EVENT);
            intent.putExtra("data", new DownloadFileEvent(message.getAttachmentType(), message.getAttachment(), getAdapterPosition()));
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        }
    }


    class ImageMessageViewHolders extends BaseMessageViewHolder {
        @BindView(R.id.image)
        ImageView image;

        ImageMessageViewHolders(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    if (pos != -1) {
                        if (!Constants.CHAT_CAB) {
                        }
                        //context.startActivity(ImageViewerActivity.newInstance(context, dataList.get(pos).getAttachment().getUrl()));
                    }
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    if (pos != -1) {
                        //onItemClick(true, pos);
                    }
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    int pos = getAdapterPosition();
                    if (pos != -1) {
                        // onItemClick(false, pos);
                    }
                    return true;
                }
            });
        }

        @Override
        protected void setData(ChatMessage message) {
            super.setData(message);
            if (message.getAttachment() != null && !TextUtils.isEmpty(message.getAttachment().getUrl())) {
                Glide.with(context).load(message.getAttachment().getUrl())
                        .apply(new RequestOptions().placeholder(R.drawable.placeholder).diskCacheStrategy(DiskCacheStrategy.ALL))
                        .into(image);
            }
        }
    }

    class LoadingViewHolders extends BaseMessageViewHolder {
        @BindView(R.id.progress)
        ProgressBar progress;

        LoadingViewHolders(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void setData(ChatMessage message) {
            super.setData(message);
            progress.setVisibility(View.VISIBLE);
        }
    }

    class VideoMessageViewHolders extends BaseMessageViewHolder {
        private File file;
        private TextView text, durationOrSize;
        private ImageView videoThumbnail, videoPlay;
        private ProgressBar progressBar;

        VideoMessageViewHolders(View itemView) {
            super(itemView);
            text = itemView.findViewById(R.id.text);
            durationOrSize = itemView.findViewById(R.id.videoSize);
            videoThumbnail = itemView.findViewById(R.id.videoThumbnail);
            videoPlay = itemView.findViewById(R.id.videoPlay);
            progressBar = itemView.findViewById(R.id.progressBar);

            itemView.findViewById(R.id.videoPlay).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    if (pos != -1)
                        downloadFile(pos);
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    if (pos != -1) {
                        //onItemClick(true, pos);
                    }
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    int pos = getAdapterPosition();
                    if (pos != -1) {
                        //onItemClick(false, pos);
                    }
                    return true;
                }
            });
        }

        @Override
        protected void setData(ChatMessage message) {
            super.setData(message);
            boolean loading = message.getAttachment().getUrl().equals("loading");
            progressBar.setVisibility(loading ? View.VISIBLE : View.GONE);
            videoPlay.setVisibility(loading ? View.GONE : View.VISIBLE);

            text.setText(message.getAttachment().getName());
            Glide.with(context).load(message.getAttachment().getData()).apply(new RequestOptions().placeholder(R.drawable.ic_video_24dp).centerCrop()).into(videoThumbnail);

            file = new File(Environment.getExternalStorageDirectory() + "/"
                    +
                    context.getString(R.string.app_name) + "/" + AttachmentTypes1.getTypeName(message.getAttachmentType()) + (isMine() ? "/.sent/" : "")
                    , message.getAttachment().getName());
            videoPlay.setImageDrawable(ContextCompat.getDrawable(context, file.exists() ? R.drawable.ic_play_circle_outline : R.drawable.ic_file_download_40dp));
            if (file.exists()) {
//            Uri uri = Uri.fromFile(file);
//            try {
//                MediaMetadataRetriever mmr = new MediaMetadataRetriever();
//                mmr.setDataSource(context, uri);
//                String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
//                long millis = Long.parseLong(durationStr);
//                durationOrSize.setText(TimeUnit.MILLISECONDS.toMinutes(millis) + ":" + TimeUnit.MILLISECONDS.toSeconds(millis));
//                Log.e("CHECK", String.valueOf(millis));
//                mmr.release();
//            } catch (RuntimeException e) {
//                Cursor cursor = MediaStore.Video.query(context.getContentResolver(), uri, new
//                        String[]{MediaStore.Video.VideoColumns.DURATION});
//                long duration = 0;
//                if (cursor != null && cursor.moveToFirst()) {
//                    duration = cursor.getLong(cursor.getColumnIndex(MediaStore.Video.VideoColumns.DURATION));
//                    Log.e("CHECK", String.valueOf(duration));
//                    durationOrSize.setText(TimeUnit.MILLISECONDS.toMinutes(duration) + ":" + TimeUnit.MILLISECONDS.toSeconds(duration));
//                }
//                if (cursor != null && !cursor.isClosed())
//                    cursor.close();
//            }
            } else
                durationOrSize.setText(FileUtils.getReadableFileSize(message.getAttachment().getBytesCount()));
        }

        void downloadFile(int pos) {
            if (!Constants.CHAT_CAB)
                if (file.exists()) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    Uri uri = MyFileProvider.getUriForFile(context,
                            context.getString(R.string.authority),
                            file);
                    intent.setDataAndType(uri, FileUtils.getMimeType(context, uri)); //storage path is path of your vcf file and vFile is name of that file.
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    context.startActivity(intent);
                } else if (!isMine()) {
                }
                //broadcastDownloadEvent(dataList.get(pos));
                else
                    Toast.makeText(context, "File unavailable", Toast.LENGTH_SHORT).show();
        }
    }
}