package com.verbosetech.weshare.chat.sendlocation.listners;

import com.verbosetech.weshare.chat.sendlocation.models.NearByPlace;
import com.verbosetech.weshare.chat.sendlocation.models.PlaceModel;

import java.util.ArrayList;

/*
 * Created by developer on 4/1/19.
 */
public interface NearByPlacesResults {

    void getListOfNearByPlace(ArrayList<NearByPlace> nearByPlaceArrayList);
}
