package com.verbosetech.weshare.chat.sendlocation.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Location;
import android.util.Log;

import com.verbosetech.weshare.BuildConfig;
import com.verbosetech.weshare.R;
import com.verbosetech.weshare.chat.sendlocation.listners.AutoCompleteResults;
import com.verbosetech.weshare.chat.sendlocation.listners.LocationInfoListner;
import com.verbosetech.weshare.chat.sendlocation.listners.NearByPlacesResults;
import com.verbosetech.weshare.chat.sendlocation.listners.PlaceInfoListner;
import com.verbosetech.weshare.chat.sendlocation.listners.RetrofitService;
import com.verbosetech.weshare.chat.sendlocation.models.GeocoderLocation;
import com.verbosetech.weshare.chat.sendlocation.models.NearByPlace;
import com.verbosetech.weshare.chat.sendlocation.models.PlaceModel;
import com.verbosetech.weshare.chat.sendlocation.network.Listener;
import com.verbosetech.weshare.chat.sendlocation.network.LocationRetrofitClient;
import com.verbosetech.weshare.util.Helper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;

/*
 * Created by developer on 4/1/19.
 */
public final class LocationUtils {

    public static void getLocationInfoByLatLong(Context context, double lat, double lng, final LocationInfoListner locationInfoListner) {

        boolean sensor = true;
        String location = lat + "," + lng;
        String key = BuildConfig.MAP_KEY;

        Call<ResponseBody> responseBodyCall = LocationRetrofitClient.create().getLocationInfo(sensor, location, key);
        responseBodyCall.enqueue(new Listener(new RetrofitService() {
            @Override
            public void onSuccess(String result, int pos, Throwable t) {
                if (pos == 0) {
                    try {
                        JSONObject main = new JSONObject(result);
                        Log.e("checkJson", "----->" + main.toString());
                        parseGeoLocation(main, locationInfoListner);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        }, null, false, context));

    }


    private static void parseGeoLocation(JSONObject object, LocationInfoListner locationInfoListner) {
        GeocoderLocation location = new GeocoderLocation();
        if (object != null) {
            String status;
            try {
                status = object.getString("status");

                if (status.equals("OK")) {
                    JSONArray array = object.getJSONArray("results");
                    if (array.length() > 0) {
                        JSONObject address = array.getJSONObject(0);
                        JSONArray address_components = address.getJSONArray("address_components");
                        String fullAddress = "";
                        for (int i = 0; i < address_components.length(); i++) {
                            JSONObject in_address = address_components.getJSONObject(i);
                            if (in_address.has("long_name")) {
                                fullAddress += in_address.getString("long_name") + ",";
                            }

                            JSONArray types = in_address.getJSONArray("types");
                            if (types.getString(0).equals("locality")) {
                                location.locality = in_address.getString("long_name");
                                Log.i("locality", location.locality);
                            } else if (types.getString(0).equals("administrative_area_level_2")) {
                                location.district = in_address.getString("long_name");
                                Log.i("subadmin", location.district);
                            } else if (types.getString(0).equals("administrative_area_level_1")) {
                                location.state = in_address.getString("long_name");
                                Log.i("admin", location.state);
                            } else if (types.getString(0).equals("country")) {
                                location.Country = in_address.getString("long_name");
                                Log.i("country", location.Country);
                            } else if (types.getString(0).equals("postal_code")) {
                                location.postalCode = in_address.getString("long_name");
                                Log.i("postal_code", location.postalCode);
                            }
                        }
                        if (fullAddress.length() > 0)
                            fullAddress = fullAddress.substring(0, fullAddress.length() - 1);
                        location.fullAdress = fullAddress;
                        JSONObject latlng = address.getJSONObject("geometry").getJSONObject("location");
                        if (latlng != null) {
                            location.lat = latlng.getDouble("lat");
                            location.lng = latlng.getDouble("lng");
                            Log.i("location", location.lat + "//" + location.lng);
                        }

                    }
                }
            } catch (JSONException e) {

                e.printStackTrace();
            }
        }
        locationInfoListner.requestsLocationInfo(location);
    }


    public static void getAutoCompleteResults(Context ctx,
                                              String input,
                                              Location location,
                                              final AutoCompleteResults autoCompleteResults
    ) {
        try {
            boolean sensor = false;
            String inputs = URLEncoder.encode(input, "utf8");
            String locations = URLEncoder.encode(getLatLng(ctx, location), "utf8");
            String key = BuildConfig.MAP_KEY;
            int radius = 1000;

            Call<ResponseBody> responseBodyCall =
                    LocationRetrofitClient.create().getAutoCompleteResults(sensor
                            , inputs, locations, radius, key);

            responseBodyCall.enqueue(new Listener(new RetrofitService() {
                @Override
                public void onSuccess(String result, int pos, Throwable t) {
                    if (pos == 0) {
                        parseAutoCompleteResults(result, autoCompleteResults);
                        Log.d("dkjgfjabdfbkabskjdbasbd", result);
                    } else {
                    }

                }
            }, null, false, ctx));


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


    }

    private static void parseAutoCompleteResults(String result, AutoCompleteResults autoCompleteResults) {
        ArrayList<PlaceModel> resultList = null;

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(result);
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<>(predsJsonArray.length());

            for (int i = 0; i < predsJsonArray.length(); i++) {
                PlaceModel model = new PlaceModel();
                model.description = predsJsonArray.getJSONObject(i).getString("description");
                model.placeid = predsJsonArray.getJSONObject(i).getString("place_id");
                resultList.add(model);
            }
            autoCompleteResults.getListOfPlace(resultList);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private static String getLatLng(Context context, Location currentLocation) {
        // If the location is valid
        if (currentLocation != null) {

            // Return the latitude and longitude as strings
            return context.getString(R.string.latitude_longitude,
                    currentLocation.getLatitude(),
                    currentLocation.getLongitude());
        } else {

            // Otherwise, return the empty string
            return "";
        }
    }


    public static void getNearByPlaces(Context ctx,
                                       Location location,
                                       final NearByPlacesResults nearByPlacesResults) {
        boolean sensor = true;
        String locations = location.getLatitude() + "," + location.getLongitude();
        int radius = 5000;
        String key = BuildConfig.MAP_KEY;

        Call<ResponseBody> responseBodyCall =
                LocationRetrofitClient.create().getNearByPlaces(sensor
                        , locations, radius, key);
        responseBodyCall.enqueue(new Listener(new RetrofitService() {
            @Override
            public void onSuccess(String result, int pos, Throwable t) {
                if (pos == 0) {
                    parseNearByLPlacesResult(result, nearByPlacesResults);
                    Log.d("dkjgfjabdfbkabskjdbasbd", result);
                }

            }
        }, null, false, ctx));


    }


    private static void parseNearByLPlacesResult(String results, NearByPlacesResults nearByPlacesResults) {
        ArrayList<NearByPlace> nearByPlaceArrayList = new ArrayList<>();

        try {
            JSONObject result = new JSONObject(results);
            JSONArray jsonArray = result.getJSONArray("results");
            if (result.getString(LocationConstants.STATUS).equalsIgnoreCase(LocationConstants.OK)) {


                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject place = jsonArray.getJSONObject(i);
                    NearByPlace byPlace = new NearByPlace();

                    byPlace.setId(place.getString(LocationConstants.SUPERMARKET_ID));
                    byPlace.setPlace_id(place.getString(LocationConstants.PLACE_ID));
                    if (!place.isNull(LocationConstants.NAME)) {
                        byPlace.setPlaceName(place.getString(LocationConstants.NAME));
                    }
                    if (!place.isNull(LocationConstants.VICINITY)) {
                        byPlace.setVicinity(place.getString(LocationConstants.VICINITY));
                    }
                    byPlace.setLatitude(place.getJSONObject(LocationConstants.GEOMETRY).getJSONObject(LocationConstants.LOCATION)
                            .getDouble(LocationConstants.LATITUDE));
                    byPlace.setLongitude(place.getJSONObject(LocationConstants.GEOMETRY).getJSONObject(LocationConstants.LOCATION)
                            .getDouble(LocationConstants.LONGITUDE));
                    byPlace.setReference(place.getString(LocationConstants.REFERENCE));
                    byPlace.setIcon(place.getString(LocationConstants.ICON));
                    nearByPlaceArrayList.add(byPlace);
                }

            }
            nearByPlacesResults.getListOfNearByPlace(nearByPlaceArrayList);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public static void getPlaceInfoByPlaceId(Context context, String placeId, final PlaceInfoListner placeInfoListner) {

        try {
            final ProgressDialog pdg = Helper.getProgressDialog(context);
            String place = URLEncoder.encode(placeId, "utf8");
            String key = BuildConfig.MAP_KEY;

            Call<ResponseBody> responseBodyCall =
                    LocationRetrofitClient.create().getPlaceInfo(place
                            , key);

            responseBodyCall.enqueue(new Listener(new RetrofitService() {
                @Override
                public void onSuccess(String result, int pos, Throwable t) {
                    if (pos == 0) {
                        try {
                            placeInfoListner.getPlaceInfo(parsePlaceInfo(result));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("dkjgfjabdfbkabskjdbasbd", result);
                    }
                    pdg.dismiss();
                }
            }, null, false, context));


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }

    private static Address parsePlaceInfo(String result) throws JSONException {

        JSONObject jObj = new JSONObject(result);
        Address fullAddress = new Address(Locale.getDefault());
        JSONObject jObj2, jObj3;

        // if (jArray.length() > 0) {
        // jObj1 = jObj;
        if (jObj.has(LocationConstants.STATUS_KEY)
                && jObj.getString(LocationConstants.STATUS_KEY).equalsIgnoreCase("ok")) {
            JSONObject jArray = jObj.getJSONObject(LocationConstants.RESULT_KEY);
            if (jArray != null) {
                /* if (jArray.has("name")) {
                    fullAddress.setAddressLine(0, jArray.getString("name"));
                }
                if (jArray.has("formatted_address")) {
                    address = jArray.getString("formatted_address");
                    if (fullAddress.getMaxAddressLineIndex() != -1)
                        fullAddress.setAddressLine(1, address);
                    else fullAddress.setAddressLine(0, address);
                }*/
                if (jArray.has(LocationConstants.GEOMETRY_KEY)) {


                    jObj2 = jArray.getJSONObject(LocationConstants.GEOMETRY_KEY);

                    if (jObj2.has(LocationConstants.LOCATION_KEY)) {
                        jObj3 = jObj2.getJSONObject(LocationConstants.LOCATION_KEY);
                        fullAddress.setLatitude(jObj3.getDouble(LocationConstants.LATITUDE_KEY));
                        fullAddress.setLongitude(jObj3.getDouble(LocationConstants.LONGITUDE_KEY));
                        return fullAddress;
                    }
                }
            }
        }
        return fullAddress;
    }


}
