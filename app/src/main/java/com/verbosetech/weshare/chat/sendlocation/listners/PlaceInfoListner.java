package com.verbosetech.weshare.chat.sendlocation.listners;

import android.location.Address;

/*
 * Created by developer on 6/1/19.
 */
public interface PlaceInfoListner {

    void getPlaceInfo(Address address);
}
