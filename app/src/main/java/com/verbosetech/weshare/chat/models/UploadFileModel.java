package com.verbosetech.weshare.chat.models;

import com.verbosetech.weshare.firestore.FireStoreConstants;

import lombok.Data;

/*
 * Created by developer on 27/12/18.
 */
@Data
public class UploadFileModel {
    private Attachment attachment;
    private int attachmentType;
    private String attachmentFilePath,attachmentRecipientId,attachmentChatChild;
}
