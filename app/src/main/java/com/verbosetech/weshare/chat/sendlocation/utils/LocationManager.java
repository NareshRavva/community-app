package com.verbosetech.weshare.chat.sendlocation.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

public class LocationManager implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener, GpsStatus.Listener, ResultCallback<LocationSettingsResult> {

    public static LocationManager instance;
    private Context context;

    private GoogleApiClient mGoogleApiClient;

    private int durationInSecs = 30;

    private android.location.LocationManager locationManager;
    private int used;
    private int total;

    @Override
    public void onGpsStatusChanged(int event) {
        getSatInfo();
    }


    public interface LocationChangeListener {

        public void OnLocationChange(Location location);

    }

    private LocationManager(final Context context) {
        this.context = context;

        locationManager = (android.location.LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        Handler h = new Handler(Looper.getMainLooper());

        h.post(new Runnable() {
            @Override
            public void run() {
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                locationManager.addGpsStatusListener(LocationManager.this);
            }
        });

        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();


    }

    /***
     * starts getting fused location for every durationInSecs
     * @param listener
     * @param durationInSecs
     */
    public void startListening(LocationChangeListener listener, int durationInSecs) {

        if (mGoogleApiClient != null) {
            this.durationInSecs = durationInSecs;
            mGoogleApiClient.connect();
        }
    }

    /***
     * Stops getting fused location
     */
    public void stopListening() {

        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }

    }

    /***
     * Creates object
     * @param context context of the class
     * @return instance
     */
    public static LocationManager getInstance(Context context) {

        if (instance == null)
            instance = new LocationManager(context);

        return instance;

    }

    @Override
    public void onLocationChanged(Location location) {
        // TODO Auto-generated method stub
        // mLocationChangeListener.OnLocationChange(location);
    }

    @Override
    public void onConnectionFailed(ConnectionResult arg0) {
        // TODO Auto-generated method stub

    }

    public class TSLocation {

        public double lat;
        public double lng;

    }


    @Override
    public void onConnected(Bundle arg0) {
        // TODO Auto-generated method stub
        LocationRequest mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(1000 * durationInSecs);
        mLocationRequest.setSmallestDisplacement(0);

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling

            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

    }

    public Location getCurrentLocation() {


        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return null;
        }
        Location fusedLocation = null;
        if (mGoogleApiClient != null) {
            fusedLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }
        Location gpsLocation = locationManager.getLastKnownLocation(android.location.LocationManager.GPS_PROVIDER);
        Location networkLocation = locationManager.getLastKnownLocation(android.location.LocationManager.NETWORK_PROVIDER);
        //ChatLocation passiveLocation = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

        Location betterLocation = getBetterLocation(gpsLocation, fusedLocation);
        if (betterLocation == null) {
            betterLocation = getBetterLocation2(betterLocation, networkLocation);
        }

        //getBetterLocation(getBetterLocation(gpsLocation, networkLocation), passiveLocation);


        // ChatLocation finalLocation = getBetterLocation(betterLocation, fusedLocation);
        return betterLocation;

    }

    public boolean isLocationOn() {

        return locationManager.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(android.location.LocationManager.NETWORK_PROVIDER);

    }

    /***
     * gives the accurate location between fused location and gps location
     * @param loc1 first location
     * @param loc2 second location
     * @return location object
     */
    protected Location getBetterLocation(Location loc1, Location loc2) {
        if (loc1 == null && loc2 != null) {

            return loc2;
        } else if (loc2 == null && loc1 != null) {

            return loc1;
        } else if (loc1 != null) {

            if (loc1.getAccuracy() < loc2.getAccuracy()) {
                return loc1;
            } else {
                return loc2;
            }
        } else {
            return null;
        }


    }

    protected Location getBetterLocation2(Location loc1, Location loc2) {
        if (loc1 == null && loc2 != null) {
            return loc2;
        } else if (loc2 == null && loc1 != null) {
            return loc1;
        } else if (loc1 != null && loc2 != null) {
            if (loc1.getAccuracy() < loc2.getAccuracy()) {
                return loc1;
            } else {
                return loc2;
            }
        } else {
            return null;
        }


    }

    /***
     * calculates the number of satellites used to get location.
     */
    private void getSatInfo() {

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Iterable<GpsSatellite> sats = locationManager.getGpsStatus(null).getSatellites();
        for (GpsSatellite s : sats) {
            if (s.usedInFix()) {
                used++;
            }
            total++;
        }

    }

    @Override
    public void onConnectionSuspended(int arg0) {
        // TODO Auto-generated method stub
    }

    public void locationChecker() {
        if (mGoogleApiClient != null) {

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(1000 * durationInSecs);
            locationRequest.setSmallestDisplacement(0);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);
            builder.setAlwaysShow(true);
            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
            result.setResultCallback(this);
        }
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult result1) {
        final Status status = result1.getStatus();
        final LocationSettingsStates state = result1.getLocationSettingsStates();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                // All location settings are satisfied. The client can initialize location
                // requests here.
                Toast.makeText(context, "LocationSuccess", Toast.LENGTH_SHORT).show();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                // ChatLocation settings are not satisfied. But could be fixed by showing the user
                // a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    status.startResolutionForResult(
                            (Activity) context, 1000);
                } catch (IntentSender.SendIntentException e) {
                    // Ignore the error.
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // ChatLocation settings are not satisfied. However, we have no way to fix the
                // settings so we won't show the dialog.
                break;
        }
    }
}