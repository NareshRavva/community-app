package com.verbosetech.weshare.chat.sendlocation.models;

import lombok.Data;

/**
 * Created by thrymr on 2/9/16.
 */
@Data
public class PlaceModel {

    public String description,placeid;
}
