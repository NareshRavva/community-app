package com.verbosetech.weshare.chat.models;

import io.realm.annotations.Ignore;
import lombok.Data;

/*
 * Created by developer on 26/12/18.
 */
@Data
public class ChatMessage {
    private String body, senderName, id, senderId, recipientId;
    private long date;
    private boolean delivered = false, sent = false, isLoading;
    private
    @AttachmentTypes.AttachmentType
    int attachmentType;
    private Attachment attachment;
    @Ignore
    private boolean selected;

    private ChatLocation location;

    public ChatMessage() {

    }

    public ChatMessage(int attachmentType) {
        this.attachmentType = attachmentType;
    }
}
