package com.verbosetech.weshare.chat.adapters;

import android.content.Context;
import android.graphics.Color;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.verbosetech.weshare.R;
import com.verbosetech.weshare.chat.models.AttachmentTypes;
import com.verbosetech.weshare.chat.models.ChatMessage;
import com.verbosetech.weshare.firestore.FireStoreOperations;
import com.verbosetech.weshare.model.AttachmentTypes1;
import com.verbosetech.weshare.util.FileUtils;
import com.verbosetech.weshare.util.Helper;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
 * Created by developer on 4/1/19.
 */
public class VideoMessageViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.card_view)
    CardView cardView;
    @BindView(R.id.container)
    LinearLayout ll;
    @BindView(R.id.senderName)
    TextView senderName;
    @BindView(R.id.videoThumbnail)
    ImageView videoThumbnail;
    @BindView(R.id.videoSize)
    TextView videoSize;
    @BindView(R.id.videoPlay)
    ImageView videoPlay;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.text)
    TextView text;
    @BindView(R.id.time)
    TextView time;

    private boolean isMine;

    VideoMessageViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void setData(ChatMessage message, Context context) {
        this.isMine = FireStoreOperations.getInstance().getUserId().equals(message.getSenderId());
        int _48dpInPx = Helper.dp2px(context, 48);
        if (!message.isLoading()) {
            if (message.getAttachmentType() == AttachmentTypes.NONE_TYPING)
                return;

            senderName.setVisibility(View.GONE);
            //senderName.setText(isMine() ? "You" : message.getSenderName());
            time.setText(Helper.getTime(message.getDate()));
            time.setCompoundDrawablesWithIntrinsicBounds(0, 0, isMine ? (message.isSent() ? (message.isDelivered() ? R.drawable.ic_done_all_black : R.drawable.ic_done_black) : R.drawable.ic_waiting) : 0, 0);
           // cardView.setCardBackgroundColor(ContextCompat.getColor(context, message.isSelected() ? R.color.colorDivider : R.color.colorBgLight));
            //ll.setBackgroundColor(message.isSelected() ? ContextCompat.getColor(context, R.color.colorDivider) : isMine ? Color.WHITE : ContextCompat.getColor(context, R.color.colorBgLight));
            cardView.setCardBackgroundColor(isMine ? ContextCompat.getColor(context, R.color.colorPrimary) : ContextCompat.getColor(context, R.color.colorPrimary_alpha));
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) cardView.getLayoutParams();
            if (isMine) {
                layoutParams.gravity = Gravity.END;
                layoutParams.setMargins(_48dpInPx, 0, 0, 0);
                //ll.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_chat_corner_right));
            } else {
                layoutParams.gravity = Gravity.START;
                layoutParams.setMargins(0, 0, _48dpInPx, 0);
                //ll.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_chat_corner_left));
            }
            cardView.setLayoutParams(layoutParams);
            setVideosData(message, context);
        } else {
            return;
        }
    }

    private void setVideosData(ChatMessage message, Context context) {
        boolean loading = message.getAttachment().getUrl().equals("loading");
        progressBar.setVisibility(loading ? View.VISIBLE : View.GONE);
        videoPlay.setVisibility(loading ? View.GONE : View.VISIBLE);

        text.setText(message.getAttachment().getName());
        Glide.with(context).load(message.getAttachment().getData()).apply(new RequestOptions().placeholder(R.drawable.ic_video_24dp).centerCrop()).into(videoThumbnail);

        File file = new File(Environment.getExternalStorageDirectory() + "/"
                +
                context.getString(R.string.app_name) + "/" + AttachmentTypes1.getTypeName(message.getAttachmentType()) + (isMine ? "/.sent/" : "")
                , message.getAttachment().getName());
        videoPlay.setImageDrawable(ContextCompat.getDrawable(context, file.exists() ? R.drawable.ic_play_circle_outline : R.drawable.ic_file_download_40dp));
        if (file.exists()) {
//            Uri uri = Uri.fromFile(file);
//            try {
//                MediaMetadataRetriever mmr = new MediaMetadataRetriever();
//                mmr.setDataSource(context, uri);
//                String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
//                long millis = Long.parseLong(durationStr);
//                durationOrSize.setText(TimeUnit.MILLISECONDS.toMinutes(millis) + ":" + TimeUnit.MILLISECONDS.toSeconds(millis));
//                Log.e("CHECK", String.valueOf(millis));
//                mmr.release();
//            } catch (RuntimeException e) {
//                Cursor cursor = MediaStore.Video.query(context.getContentResolver(), uri, new
//                        String[]{MediaStore.Video.VideoColumns.DURATION});
//                long duration = 0;
//                if (cursor != null && cursor.moveToFirst()) {
//                    duration = cursor.getLong(cursor.getColumnIndex(MediaStore.Video.VideoColumns.DURATION));
//                    Log.e("CHECK", String.valueOf(duration));
//                    durationOrSize.setText(TimeUnit.MILLISECONDS.toMinutes(duration) + ":" + TimeUnit.MILLISECONDS.toSeconds(duration));
//                }
//                if (cursor != null && !cursor.isClosed())
//                    cursor.close();
//            }
        } else
            videoSize.setText(FileUtils.getReadableFileSize(message.getAttachment().getBytesCount()));
    }

}
