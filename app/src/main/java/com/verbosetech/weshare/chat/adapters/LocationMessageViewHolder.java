package com.verbosetech.weshare.chat.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.verbosetech.weshare.R;
import com.verbosetech.weshare.chat.models.AttachmentTypes;
import com.verbosetech.weshare.chat.models.ChatLocation;
import com.verbosetech.weshare.chat.models.ChatMessage;
import com.verbosetech.weshare.firestore.FireStoreOperations;
import com.verbosetech.weshare.util.Helper;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
 * Created by developer on 4/1/19.
 */
public class LocationMessageViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.senderName)
    TextView senderName;
    @BindView(R.id.card_view)
    CardView cardView;
    @BindView(R.id.container)
    LinearLayout ll;

    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.tv_location_name)
    TextView tvLocationName;

    LocationMessageViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void setData(ChatMessage message, Context context) {
        boolean isMine = FireStoreOperations.getInstance().getUserId().equals(message.getSenderId());
        int _48dpInPx = Helper.dp2px(context, 48);
        if (!message.isLoading()) {
            if (message.getAttachmentType() == AttachmentTypes.NONE_TYPING)
                return;

            senderName.setVisibility(View.GONE);
            //senderName.setText(isMine() ? "You" : message.getSenderName());
            time.setText(Helper.getTime(message.getDate()));
            time.setCompoundDrawablesWithIntrinsicBounds(0, 0, isMine ? (message.isSent() ? (message.isDelivered() ? R.drawable.ic_done_all_black : R.drawable.ic_done_black) : R.drawable.ic_waiting) : 0, 0);
            cardView.setCardBackgroundColor(isMine ? ContextCompat.getColor(context, R.color.colorPrimary) : ContextCompat.getColor(context, R.color.colorPrimary_alpha));
            //cardView.setCardBackgroundColor(ContextCompat.getColor(context, message.isSelected() ? R.color.colorDivider : R.color.colorBgLight));
            //ll.setBackgroundColor(message.isSelected() ? ContextCompat.getColor(context, R.color.colorDivider) : isMine ? Color.WHITE : ContextCompat.getColor(context, R.color.colorBgLight));
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) cardView.getLayoutParams();
            if (isMine) {
                layoutParams.gravity = Gravity.END;
                layoutParams.setMargins(_48dpInPx, 0, 0, 0);
            } else {
                layoutParams.gravity = Gravity.START;
                layoutParams.setMargins(0, 0, _48dpInPx, 0);
            }
            cardView.setLayoutParams(layoutParams);


            ChatLocation chatLocation = message.getLocation();
            if (chatLocation != null) {
                tvLocationName.setText(chatLocation.getName() + "\n" + chatLocation.getLat() + "," + chatLocation.getLang());
            }
            if (message.getAttachment() != null && !TextUtils.isEmpty(message.getAttachment().getUrl())) {
                Glide.with(context).load(message.getAttachment().getUrl())
                        .apply(new RequestOptions().placeholder(R.drawable.placeholder).diskCacheStrategy(DiskCacheStrategy.ALL))
                        .into(image);
            }
        } else {
            return;
        }
    }

}
