package com.verbosetech.weshare.chat.activity;

import android.content.Intent;
import android.location.Address;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.iceteck.silicompressorr.SiliCompressor;
import com.kbeanie.multipicker.api.AudioPicker;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ContactPicker;
import com.kbeanie.multipicker.api.FilePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.VideoPicker;
import com.kbeanie.multipicker.api.callbacks.ContactPickerCallback;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.callbacks.VideoPickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenContact;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.kbeanie.multipicker.api.entity.ChosenVideo;
import com.vanniktech.emoji.EmojiEditText;
import com.vanniktech.emoji.EmojiPopup;
import com.vanniktech.emoji.listeners.OnEmojiPopupShownListener;
import com.verbosetech.weshare.R;
import com.verbosetech.weshare.chat.ChatOperations;
import com.verbosetech.weshare.chat.adapters.ChatAdapter;
import com.verbosetech.weshare.chat.listners.OnMessageItemClick;
import com.verbosetech.weshare.chat.models.AttachmentTypes;
import com.verbosetech.weshare.chat.models.ChatHistoryModel;
import com.verbosetech.weshare.chat.models.ChatLocation;
import com.verbosetech.weshare.chat.models.ChatMessage;
import com.verbosetech.weshare.chat.models.MessageModel;
import com.verbosetech.weshare.chat.sendlocation.activity.SendLocationActivity;
import com.verbosetech.weshare.firestore.FireStoreConstants;
import com.verbosetech.weshare.firestore.FireStoreOperations;
import com.verbosetech.weshare.listener.OnResultListner;
import com.verbosetech.weshare.model.Post;
import com.verbosetech.weshare.util.Constants;
import com.verbosetech.weshare.util.Helper;
import com.verbosetech.weshare.util.Logger;

import java.io.File;
import java.util.List;

import javax.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/*
 * Created by developer on 26/12/18.
 */
public class ChatActivity extends SuperChatActivity implements ImagePickerCallback, VideoPickerCallback {
    @BindView(R.id.rootView)
    LinearLayout rootView;
    @BindView(R.id.chatToolbar)
    Toolbar chatToolbar;
    @BindView(R.id.userImage)
    ImageView userImage;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.sendContainer)
    LinearLayout sendContainer;
    @BindView(R.id.attachment_emoji)
    ImageView attachmentEmoji;
    @BindView(R.id.new_message)
    EmojiEditText newMessage;
    @BindView(R.id.add_attachment)
    ImageView addAttachment;
    @BindView(R.id.send)
    ImageView send;
    @BindView(R.id.add_attachment_layout)
    TableLayout addAttachmentLayout;
    @BindView(R.id.attachment_camera)
    TextView attachmentCamera;
    @BindView(R.id.attachment_gallery)
    TextView attachmentGallery;
    @BindView(R.id.attachment_video)
    TextView attachmentVideo;
    @BindView(R.id.attachment_location)
    TextView attachmentLocation;
    @BindView(R.id.attachment_contact)
    TextView attachmentContact;


    private LinearLayoutManager linearLayoutManager;
    private ChatHistoryModel consersation = new ChatHistoryModel();
    private ChatAdapter chatAdapter;
    private String TAG = this.getClass().getSimpleName();
    private EmojiPopup emojIcon;

    private String pickerPath;
    private ImagePicker imagePicker;
    private CameraImagePicker cameraPicker;
    private FilePicker filePicker;
    private AudioPicker audioPicker;
    private VideoPicker videoPicker;
    private ContactPicker contactPicker;
    private ChatOperations chatOperations;

    public int PLACE_ACTIVITY_INTNET = 6222;
    private Post post;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);
        ButterKnife.bind(this);

        String postData = getIntent().getStringExtra(Constants.POST_DATA);
        if (!TextUtils.isEmpty(postData)) {
            Gson gson = new Gson();
            post = gson.fromJson(postData, Post.class);
        }

        initUi();
        registerUserUpdates();
        getListOfChatMessages();


        emoJiSetUp();

        newMessage.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (addAttachmentLayout.getVisibility() == View.VISIBLE) {
                    addAttachmentLayout.setVisibility(View.GONE);
                    addAttachment.animate().setDuration(400).rotationBy(-45).start();
                }
                return false;
            }
        });
    }

    private void emoJiSetUp() {
        emojIcon = EmojiPopup.Builder.fromRootView(rootView).setOnEmojiPopupShownListener(new OnEmojiPopupShownListener() {
            @Override
            public void onEmojiPopupShown() {
                if (addAttachmentLayout.getVisibility() == View.VISIBLE) {
                    addAttachmentLayout.setVisibility(View.GONE);
                    addAttachment.animate().setDuration(400).rotationBy(-45).start();
                }
            }
        }).build(newMessage);
    }

    private void initUi() {
        chatToolbar.setTitleTextAppearance(this, R.style.MontserratBoldTextAppearance);
        setSupportActionBar(chatToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_keyboard_arrow_left);
        }
        chatOperations = new ChatOperations(this);
    }

    private void getListOfChatMessages() {
        if (post == null) {
            return;
        }

        Logger.getInstance().d("---getCommunityId>" +
                post.getCommunityId() + "\n" +
                FireStoreOperations.getInstance().getUserId() + "\n" + post.getUserMetaData().getId());
        setViews();
        CollectionReference cr = FirebaseFirestore.getInstance().
                collection(FireStoreConstants.TABLE_USERS).
                document(post.getCommunityId()).
                collection(FireStoreConstants.USERS_DATA).
                document(FireStoreOperations.getInstance().getUserId()).
                collection(FireStoreConstants.USER_CHAT_HISTORY).
                document(post.getUserMetaData().getId()).
                collection(FireStoreConstants.MESSAGES);
        cr.orderBy("date", Query.Direction.ASCENDING).
                addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot documentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.i(TAG, "listen:error" + e);
                            return;
                        }

                        for (DocumentChange dc : documentSnapshots.getDocumentChanges()) {
                            switch (dc.getType()) {
                                case ADDED:
                                    Logger.getInstance().i(TAG + "New city: " + dc.getDocument().getData() + "----" + dc.getDocument().toObject(MessageModel.class));
                                    consersation.getListMessageData().add(dc.getDocument().toObject(ChatMessage.class));
                                    chatAdapter.notifyDataSetChanged();
                                    linearLayoutManager.scrollToPosition(consersation.getListMessageData().size() - 1);
                                    break;
                                case MODIFIED:
                                    //showTyping(false);
                                    Logger.getInstance().i(TAG + "Modified city: " + dc.getDocument().getData());
                                    updateItem(dc.getDocument().toObject(ChatMessage.class));
                                    break;
                                case REMOVED:
                                    Logger.getInstance().i(TAG + "Removed city: " + dc.getDocument().getData());
                                    break;
                            }

                        }
                    }
                });
    }

    private void updateItem(ChatMessage chatMessage) {
        if (chatMessage != null && consersation.getListMessageData().size() > 0) {
            for (int i = 0; i < consersation.getListMessageData().size(); i++) {
                if (consersation.getListMessageData().get(i).getId().equalsIgnoreCase(chatMessage.getId())) {
                    consersation.getListMessageData().set(i, chatMessage);
                    chatAdapter.notifyItemChanged(i);
                    break;
                }
            }
        }
    }

    private void showTyping(boolean typing) {
        if (consersation.getListMessageData() != null && consersation.getListMessageData().size() > 0) {
            boolean lastIsTyping = consersation.getListMessageData().get(consersation.getListMessageData().size() - 1).getAttachmentType() == AttachmentTypes.NONE_TYPING;
            if (typing && !lastIsTyping) {//if last message is not Typing
                consersation.getListMessageData().add(new ChatMessage(AttachmentTypes.NONE_TYPING));
                chatAdapter.notifyItemInserted(consersation.getListMessageData().size() - 1);
                recyclerView.scrollToPosition(chatAdapter.getItemCount() - 1);
            } else if (lastIsTyping && consersation.getListMessageData().size() > 0) {//If last is typing and there is a message in list
                consersation.getListMessageData().remove(consersation.getListMessageData().size() - 1);
                chatAdapter.notifyItemRemoved(consersation.getListMessageData().size());
            }
        }
    }

    private void setViews() {
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        consersation = new ChatHistoryModel();
        chatAdapter = new ChatAdapter(this, consersation, new OnMessageItemClick() {

            @Override
            public void OnMessageClick(ChatMessage message, int position) {

            }

            @Override
            public void OnMessageLongClick(ChatMessage message, int position) {

            }
        });
        recyclerView.setAdapter(chatAdapter);
    }

    private void registerUserUpdates() {
        //Publish logged in user's typing status
        newMessage.addTextChangedListener(new TextWatcher() {
            CountDownTimer timer = null;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                send.setImageDrawable(ContextCompat.
                        getDrawable(ChatActivity.this, s.length() == 0 ? R.drawable.ic_keyboard_voice_24dp : R.drawable.ic_send_message));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    @OnClick({R.id.send, R.id.add_attachment,
            R.id.attachment_gallery,
            R.id.attachment_camera,
            R.id.attachment_video,
            R.id.attachment_location,
            R.id.attachment_contact})
    void onItemClick(View view) {
        if (view == send) {
            chatOperations.sendMessage(newMessage.getText().toString(),
                    AttachmentTypes.NONE_TEXT,
                    null,
                    null, post);
            newMessage.setText("");
        } else if (view == addAttachment) {
            Helper.closeKeyboard(this, view);
            if (addAttachmentLayout.getVisibility() == View.VISIBLE) {
                addAttachmentLayout.setVisibility(View.GONE);
                addAttachment.animate().setDuration(400).rotationBy(-45).start();
            } else {
                addAttachmentLayout.setVisibility(View.VISIBLE);
                addAttachment.animate().setDuration(400).rotationBy(45).start();
                emojIcon.dismiss();
            }
        } else if (view == attachmentGallery) {
            openImagePick();
        } else if (view == attachmentCamera) {
            openImageClick();
        } else if (view == attachmentVideo) {
            openVideoPicker();
        } else if (view == attachmentLocation) {
            openLocationsActivity();
        } else if (view == attachmentContact) {
            openContactPicker();
        }

    }

    //Contact share open intent
    private void openContactPicker() {
       /* Uri uri = Uri.parse("content://contacts");
        Intent intent = new Intent(Intent.ACTION_PICK, uri);
        intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
        startActivityForResult(intent, REQUEST_CONTACT);*/
        if (permissionsAvailable(permissionsContacts)) {
            contactPicker = getContactPicker();
            contactPicker.pickContact();
        } else {
            ActivityCompat.requestPermissions(this, permissionsContacts, 48);
        }
    }

    private ContactPicker getContactPicker() {
        ContactPicker picker = new ContactPicker(this);
        picker.setContactPickerCallback(new ContactPickerCallback() {
            @Override
            public void onContactChosen(ChosenContact chosenContact) {
                hideAttachment();
                chatOperations.sendMessage(chosenContact.getDisplayName(), AttachmentTypes.CONTACT, null, null, post);

            }

            @Override
            public void onError(String s) {

            }
        });
        return picker;
    }

    private void openLocationsActivity() {
        startActivityForResult(new Intent(this, SendLocationActivity.class), PLACE_ACTIVITY_INTNET);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 25:
                if (permissionsAvailable(permissions))
                    //openAudioPicker();
                    break;
            case 36:
                if (permissionsAvailable(permissions))
                    openImagePick();
                break;
            case 47:
                if (permissionsAvailable(permissions))
                    openImageClick();
                break;
            case 41:
                if (permissionsAvailable(permissions))
                    openVideoPicker();
                break;
            case 48:
                if (permissionsAvailable(permissions))
                    openContactPicker();
                break;
        }
    }

    public void openImagePick() {
        if (permissionsAvailable(permissionsStorage)) {
            imagePicker = new ImagePicker(this);
            imagePicker.shouldGenerateMetadata(true);
            imagePicker.shouldGenerateThumbnails(true);
            imagePicker.setImagePickerCallback(this);
            imagePicker.pickImage();

        } else {
            ActivityCompat.requestPermissions(this, permissionsStorage, 36);
        }
    }

    void openImageClick() {
        if (permissionsAvailable(permissionsCamera)) {
            cameraPicker = new CameraImagePicker(this);
            cameraPicker.shouldGenerateMetadata(true);
            cameraPicker.shouldGenerateThumbnails(true);
            cameraPicker.setImagePickerCallback(this);
            pickerPath = cameraPicker.pickImage();
        } else {
            ActivityCompat.requestPermissions(this, permissionsCamera, 47);
        }
    }

    private void openVideoPicker() {
        if (permissionsAvailable(permissionsStorage)) {
            videoPicker = new VideoPicker(this);
            videoPicker.shouldGenerateMetadata(true);
            videoPicker.shouldGeneratePreviewImages(true);
            videoPicker.setVideoPickerCallback(this);
            videoPicker.pickVideo();
        } else {
            ActivityCompat.requestPermissions(this, permissionsStorage, 41);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == AppCompatActivity.RESULT_OK) {

            if (requestCode == PLACE_ACTIVITY_INTNET) {
                getShareLocation(data);
                return;
            }
            switch (requestCode) {
                case Picker.PICK_IMAGE_DEVICE:
                    if (imagePicker == null) {
                        imagePicker = new ImagePicker(this);
                        imagePicker.setImagePickerCallback(this);
                    }
                    imagePicker.submit(data);
                    break;
                case Picker.PICK_IMAGE_CAMERA:
                    if (cameraPicker == null) {
                        cameraPicker = new CameraImagePicker(this);
                        cameraPicker.setImagePickerCallback(this);
                        cameraPicker.reinitialize(pickerPath);
                    }
                    cameraPicker.submit(data);
                    break;
                case Picker.PICK_VIDEO_DEVICE:
                    if (videoPicker == null) {
                        videoPicker = new VideoPicker(this);
                        videoPicker.setVideoPickerCallback(this);
                    }
                    videoPicker.submit(data);
                    break;
                case Picker.PICK_FILE:
                    filePicker.submit(data);
                    break;
                case Picker.PICK_AUDIO:
                    audioPicker.submit(data);
                    break;
                case Picker.PICK_CONTACT:
                    contactPicker.submit(data);
                    break;
            }
        }
    }

    private void getShareLocation(Intent data) {
        Address sourceAddressObject = data.getExtras().getParcelable("location_data");
        String mapSnap = data.getStringExtra("map_snap");
        Logger.getInstance().d("location_data-->" + sourceAddressObject + "--" + mapSnap);
        if (sourceAddressObject != null) {
            hideAttachment();
            ChatLocation chatLocation = new ChatLocation();
            chatLocation.setLat(sourceAddressObject.getLatitude());
            chatLocation.setLang(sourceAddressObject.getLongitude());
            if (!TextUtils.isEmpty(sourceAddressObject.getAddressLine(0))) {
                chatLocation.setName(sourceAddressObject.getAddressLine(0));
            }
            if (!TextUtils.isEmpty(mapSnap)) {
                chatLocation.setMapSnapFilePath(mapSnap);
                /*sendLocationMessageWithSnap(mapSnap, chatLocation);
                return;*/
            }
            chatOperations.sendMessage(null, AttachmentTypes.LOCATION, null, chatLocation, post);
        }
    }

    private void sendLocationMessageWithSnap(String mapSnap, ChatLocation chatLocation) {
        final File file = new File(mapSnap);
        if (file.exists()) {
            hideAttachment();
            final ChatMessage chatMessage = new ChatMessage();
            chatOperations.fileUploadTask(mapSnap, AttachmentTypes.LOCATION, chatLocation, null, new OnResultListner() {
                @Override
                public void onResultListner(Object isFileUpladingDone, boolean isSucess) {
                    uploadProcess(isFileUpladingDone, isSucess, chatMessage);
                    //file.delete();
                }
            }, post);

        }
    }

    @Override
    public void onImagesChosen(List<ChosenImage> list) {
        if (list != null && !list.isEmpty()) {
            Uri originalFileUri = Uri.parse(list.get(0).getOriginalPath());
            File tempFile = new File(getCacheDir(), originalFileUri.getLastPathSegment());
            try {
                uploadImage(SiliCompressor.with(this).compress(originalFileUri.toString(), tempFile));
            } catch (Exception ex) {
                uploadImage(originalFileUri.getPath());
            }
        }
    }

    @Override
    public void onError(String s) {

    }

    private void uploadImage(String filePath) {
        newFileUploadTask(filePath, AttachmentTypes.IMAGE);
    }

    private void newFileUploadTask(String filePath,
                                   @AttachmentTypes.AttachmentType final int attachmentType
    ) {
        hideAttachment();
        final ChatMessage chatMessage = new ChatMessage();
        chatOperations.fileUploadTask(filePath, attachmentType, null, null, new OnResultListner() {
            @Override
            public void onResultListner(Object isFileUpladingDone, boolean isSucess) {
                uploadProcess(isFileUpladingDone, isSucess, chatMessage);
            }
        }, post);

    }

    private void uploadProcess(Object isFileUpladingDone, boolean isSucess, ChatMessage chatMessage) {
        if (isSucess) {
            if ((boolean) isFileUpladingDone) {

                consersation.getListMessageData().remove(chatMessage);
            } else {
                chatMessage.setLoading(true);
                chatMessage.setSenderId(FireStoreOperations.getInstance().getUserId());
                chatMessage.setAttachmentType(AttachmentTypes.LOADING);
                consersation.getListMessageData().add(chatMessage);
            }

            chatAdapter.notifyDataSetChanged();
        }
    }


    @Override
    public void onVideosChosen(List<ChosenVideo> list) {
        if (list != null && !list.isEmpty() && new File(Uri.parse(list.get(0).getOriginalPath()).getPath()).length() / 1024 <= 10240) {
            hideAttachment();
            final ChatMessage chatMessage = new ChatMessage();
            chatOperations.uploadThumbnail(Uri.parse(list.get(0).getOriginalPath()).getPath(), new OnResultListner() {
                @Override
                public void onResultListner(Object isFileUpladingDone, boolean isSucess) {
                    uploadProcess(isFileUpladingDone, isSucess, chatMessage);
                }
            }, true, post);
        } else {
            Toast.makeText(this, "Please choose video less than 10MB", Toast.LENGTH_SHORT).show();
        }
    }


    private void hideAttachment() {
        if (addAttachmentLayout.getVisibility() == View.VISIBLE) {
            addAttachmentLayout.setVisibility(View.GONE);
            addAttachment.animate().setDuration(400).rotationBy(-45).start();
        }
    }

}
