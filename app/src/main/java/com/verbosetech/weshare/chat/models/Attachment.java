package com.verbosetech.weshare.chat.models;

import android.os.Parcel;
import android.os.Parcelable;

import lombok.Data;

/**
 * Created by mayank on 11/5/17.
 */
@Data
public class Attachment implements Parcelable{
    private String name, data, url;
    private long bytesCount;

    public Attachment() {
    }

    protected Attachment(Parcel in) {
        name = in.readString();
        data = in.readString();
        url = in.readString();
        bytesCount = in.readLong();
    }

    public static final Parcelable.Creator<Attachment> CREATOR = new Parcelable.Creator<Attachment>() {
        @Override
        public Attachment createFromParcel(Parcel in) {
            return new Attachment(in);
        }

        @Override
        public Attachment[] newArray(int size) {
            return new Attachment[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(data);
        dest.writeString(url);
        dest.writeLong(bytesCount);
    }

}

