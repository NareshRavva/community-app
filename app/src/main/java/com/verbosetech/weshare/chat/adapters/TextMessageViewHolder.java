package com.verbosetech.weshare.chat.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.method.LinkMovementMethod;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vanniktech.emoji.EmojiTextView;
import com.verbosetech.weshare.R;
import com.verbosetech.weshare.chat.models.AttachmentTypes;
import com.verbosetech.weshare.chat.models.ChatMessage;
import com.verbosetech.weshare.firestore.FireStoreOperations;
import com.verbosetech.weshare.util.Helper;
import com.verbosetech.weshare.util.LinkTransformationMethod;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
 * Created by developer on 3/1/19.
 */
public class TextMessageViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.senderName)
    TextView senderName;
    @BindView(R.id.card_view)
    CardView cardView;
    @BindView(R.id.container)
    LinearLayout ll;

    @BindView(R.id.text)
    EmojiTextView text;

    TextMessageViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

        text.setTransformationMethod(new LinkTransformationMethod());
        text.setMovementMethod(LinkMovementMethod.getInstance());

    }

    public void setData(ChatMessage message, Context context) {
        boolean isMine = FireStoreOperations.getInstance().getUserId().equals(message.getSenderId());
        int _48dpInPx = Helper.dp2px(context, 48);
        if (!message.isLoading()) {
            if (message.getAttachmentType() == AttachmentTypes.NONE_TYPING)
                return;

            senderName.setVisibility(View.GONE);
            //senderName.setText(isMine() ? "You" : message.getSenderName());
            time.setText(Helper.getTime(message.getDate()));
            time.setCompoundDrawablesWithIntrinsicBounds(0, 0, isMine ? (message.isSent() ? (message.isDelivered() ? R.drawable.ic_done_all_black : R.drawable.ic_done_black) : R.drawable.ic_waiting) : 0, 0);
            //cardView.setCardBackgroundColor(ContextCompat.getColor(context, message.isSelected() ? R.color.colorDivider : R.color.colorBgLight));
            //ll.setBackgroundColor(message.isSelected() ? ContextCompat.getColor(context, R.color.colorDivider) : isMine ? Color.WHITE : ContextCompat.getColor(context, R.color.colorBgLight));

            cardView.setCardBackgroundColor(isMine ? ContextCompat.getColor(context, R.color.colorPrimary) : ContextCompat.getColor(context, R.color.colorPrimary_alpha));
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) cardView.getLayoutParams();
            if (isMine) {
                layoutParams.gravity = Gravity.END;
                layoutParams.setMargins(_48dpInPx, 0, 0, 0);
                //ll.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_chat_corner_right));
            } else {
                layoutParams.gravity = Gravity.START;
                layoutParams.setMargins(0, 0, _48dpInPx, 0);
                //ll.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_chat_corner_left));
            }
            cardView.setLayoutParams(layoutParams);
            text.setText(message.getBody());
        } else {
            return;
        }

    }
}
