package com.verbosetech.weshare.chat.models;


import lombok.Data;

/**
 * The type Message model.
 */
@Data
public class MessageModel {
    public String id_receiver;
    public String id_sender;
    public String text;
    public long time_stamp;
    public String name;
    public String source;
    public String amount;

}