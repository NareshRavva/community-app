package com.verbosetech.weshare.chat.models;

import java.util.ArrayList;

import lombok.Data;


/**
 * The type Chat history model.
 */
@Data
public class ChatHistoryModel {
    private ArrayList<ChatMessage> listMessageData =new ArrayList<>();

}
