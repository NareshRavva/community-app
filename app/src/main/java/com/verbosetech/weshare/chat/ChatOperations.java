package com.verbosetech.weshare.chat;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.verbosetech.weshare.R;
import com.verbosetech.weshare.chat.models.Attachment;
import com.verbosetech.weshare.chat.models.AttachmentTypes;
import com.verbosetech.weshare.chat.models.ChatLocation;
import com.verbosetech.weshare.chat.models.ChatMessage;
import com.verbosetech.weshare.chat.models.UploadFileModel;
import com.verbosetech.weshare.firestore.FireStoreConstants;
import com.verbosetech.weshare.firestore.FireStoreOperations;
import com.verbosetech.weshare.listener.OnResultListner;
import com.verbosetech.weshare.model.AttachmentTypes1;
import com.verbosetech.weshare.model.Post;
import com.verbosetech.weshare.util.FileUtils;
import com.verbosetech.weshare.util.FirebaseUploader;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

/*
 * Created by developer on 27/12/18.
 */
public class ChatOperations {
    private Context context;

    public ChatOperations(Context context) {
        this.context = context;
    }

    public void fileUploadTask(String filePath,
                               @AttachmentTypes.AttachmentType final int attachmentType,
                               ChatLocation chatLocation,
                               final Attachment attachment,
                               OnResultListner onResultListner, Post post) {


        final File fileToUpload = new File(filePath);
        final String fileName = Uri.fromFile(fileToUpload).getLastPathSegment();

        Attachment preSendAttachment = attachment;//Create/Update attachment
        if (preSendAttachment == null) preSendAttachment = new Attachment();
        preSendAttachment.setName(fileName);
        preSendAttachment.setBytesCount(fileToUpload.length());
        preSendAttachment.setUrl("loading");

        checkAndCopy("/" + context.getString(R.string.app_name) + "/" + AttachmentTypes.getTypeName(attachmentType) + "/.sent/", fileToUpload);//Make a copy

        UploadFileModel uploadFileModel = new UploadFileModel();
        uploadFileModel.setAttachment(attachment);
        uploadFileModel.setAttachmentType(attachmentType);
        uploadFileModel.setAttachmentFilePath(filePath);
        uploadFileModel.setAttachmentRecipientId(FireStoreOperations.getInstance().getUserId());
        uploadFileModel.setAttachmentChatChild(FireStoreOperations.getInstance().getUserId());

        uploadAndSend(uploadFileModel, onResultListner, chatLocation, post);
    }

    private void uploadAndSend(final UploadFileModel uploadFileModel,
                               final OnResultListner onResultListner,
                               final ChatLocation chatLocation, final Post post) {

        final File fileToUpload = new File(uploadFileModel.getAttachmentFilePath());
        if (!fileToUpload.exists())
            return;
        final String fileName = Uri.fromFile(fileToUpload).getLastPathSegment();
        final StorageReference storageReference = FirebaseStorage.getInstance().getReference()
                .child(context.getString(R.string.app_name)).
                        child(AttachmentTypes.getTypeName(uploadFileModel.getAttachmentType())).child(fileName);
        storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                //If file is already uploaded
                Attachment attachment1 = uploadFileModel.getAttachment();
                if (attachment1 == null) attachment1 = new Attachment();
                attachment1.setName(fileName);
                attachment1.setUrl(uri.toString());
                attachment1.setBytesCount(fileToUpload.length());
                sendMessage(null, uploadFileModel.getAttachmentType(),
                        attachment1,/*, chatChild, recipientId*/chatLocation, post);
                onResultListner.onResultListner(true, true);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                //Elase upload and then send message
                FirebaseUploader firebaseUploader = new FirebaseUploader(new FirebaseUploader.UploadListener() {
                    @Override
                    public void onUploadFail(String message) {
                        Log.e("DatabaseException", message);
                    }

                    @Override
                    public void onUploadSuccess(String downloadUrl) {
                        Attachment attachment1 = uploadFileModel.getAttachment();
                        if (attachment1 == null) attachment1 = new Attachment();
                        attachment1.setName(fileToUpload.getName());
                        Log.d("Charssssssssssssssss11", downloadUrl + "");

                        attachment1.setUrl(downloadUrl);
                        attachment1.setBytesCount(fileToUpload.length());
                        sendMessage(null,
                                uploadFileModel.getAttachmentType(),
                                attachment1,/*,
                                chatChild,
                                recipientId*/null, post);
                        onResultListner.onResultListner(true, true);
                    }

                    @Override
                    public void onUploadProgress(int progress) {

                    }

                    @Override
                    public void onUploadCancelled() {

                    }
                }, storageReference);
                onResultListner.onResultListner(false, true);
                firebaseUploader.uploadOthers(context, fileToUpload);
            }
        });
    }


    private void checkAndCopy(String directory, File source) {
        //Create and copy file content
        File file = new File(Environment.getExternalStorageDirectory(), directory);
        boolean dirExists = file.exists();
        if (!dirExists)
            dirExists = file.mkdirs();
        if (dirExists) {
            try {
                file = new File(Environment.getExternalStorageDirectory() + directory, Uri.fromFile(source).getLastPathSegment());
                boolean fileExists = file.exists();
                if (!fileExists)
                    fileExists = file.createNewFile();
                if (fileExists && file.length() == 0) {
                    FileUtils.copyFile(source, file);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public void sendMessage(String messageBody,
                            @AttachmentTypes.AttachmentType int attachmentType,
                            Attachment attachment,
                            ChatLocation chatLocation, Post post) {
        String currentUser = FireStoreOperations.getInstance().getUserId();
        String recevier = post.getUserMetaData().getId();
        if (!TextUtils.isEmpty(currentUser)) {
            //Create message object
            ChatMessage message = new ChatMessage();
            message.setAttachmentType(attachmentType);
            if (attachmentType != AttachmentTypes1.NONE_TEXT)
                message.setAttachment(attachment);
            message.setBody(messageBody);
            message.setDate(System.currentTimeMillis());
            message.setSenderId(currentUser);
            message.setSenderName("naresh");
            message.setSent(true);
            message.setDelivered(false);
            message.setRecipientId(recevier);
            message.setId(currentUser);
            if (chatLocation != null) {
                message.setLocation(chatLocation);
                //setMapImage(chatLocation.getMapSnapFilePath());
            }

            CollectionReference cr = FirebaseFirestore.getInstance().
                    collection(FireStoreConstants.TABLE_USERS).
                    document(post.getCommunityId()).
                    collection(FireStoreConstants.USERS_DATA);


            cr.document(currentUser).
                    collection(FireStoreConstants.USER_CHAT_HISTORY)
                    .document(recevier).
                    collection(FireStoreConstants.MESSAGES).document()
                    .set(message);

            cr.document(recevier).
                    collection(FireStoreConstants.USER_CHAT_HISTORY).
                    document(currentUser).
                    collection(FireStoreConstants.MESSAGES).document()
                    .set(message);
        }
    }

    private void setMapImage(String mapSnapFilePath, Post post) {
        if (!TextUtils.isEmpty(mapSnapFilePath)) {
            File file = new File(mapSnapFilePath);
            if (file.exists()) {
                uploadThumbnail(mapSnapFilePath, new OnResultListner() {
                    @Override
                    public void onResultListner(Object o, boolean isSucess) {

                    }
                }, false, post);
            }
        }
    }


    public void uploadThumbnail(final String filePath,
                                final OnResultListner onResultListner,
                                boolean isFromVideo, final Post post) {
        Toast.makeText(context, "Just a moment..", Toast.LENGTH_LONG).show();
        File file = new File(filePath);
        final StorageReference storageReference = FirebaseStorage.getInstance().getReference()
                .child(context.getString(R.string.app_name)).child("video").child("thumbnail").child(file.getName() + ".jpg");
        storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                //If thumbnail exists
                Attachment attachment = new Attachment();
                attachment.setData(uri.toString());
                fileUploadTask(filePath,
                        AttachmentTypes.VIDEO,
                        null,
                        attachment,
                        onResultListner, post);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                @SuppressLint("StaticFieldLeak") AsyncTask<String, Void, Bitmap> thumbnailTask = new AsyncTask<String, Void, Bitmap>() {
                    @Override
                    protected Bitmap doInBackground(String... params) {
                        //Create thumbnail
                        return ThumbnailUtils.createVideoThumbnail(params[0], MediaStore.Video.Thumbnails.MINI_KIND);
                    }

                    @Override
                    protected void onPostExecute(Bitmap bitmap) {
                        super.onPostExecute(bitmap);
                        if (bitmap != null) {
                            //Upload thumbnail and then upload video
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                            byte[] data = baos.toByteArray();
                            UploadTask uploadTask = storageReference.putBytes(data);
                            uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                                @Override
                                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                    if (!task.isSuccessful()) {
                                        throw task.getException();
                                    }
                                    // Continue with the task to get the download URL
                                    return storageReference.getDownloadUrl();
                                }
                            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                @Override
                                public void onComplete(@NonNull Task<Uri> task) {
                                    if (task.isSuccessful()) {
                                        Uri downloadUri = task.getResult();
                                        Attachment attachment = new Attachment();
                                        attachment.setData(downloadUri.toString());
                                        fileUploadTask(filePath, AttachmentTypes.VIDEO, null, attachment, onResultListner, post);
                                    } else {
                                        fileUploadTask(filePath, AttachmentTypes.VIDEO, null, null, onResultListner, post);
                                    }
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    fileUploadTask(filePath, AttachmentTypes.VIDEO, null, null, onResultListner, post);
                                }
                            });
                        } else
                            fileUploadTask(filePath, AttachmentTypes.VIDEO, null, null, onResultListner, post);
                    }
                };
                thumbnailTask.execute(filePath);
            }
        });
    }


}
