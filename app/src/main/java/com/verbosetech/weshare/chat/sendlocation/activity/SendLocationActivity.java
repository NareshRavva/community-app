package com.verbosetech.weshare.chat.sendlocation.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Address;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.verbosetech.weshare.R;
import com.verbosetech.weshare.chat.sendlocation.adapter.SearchPlaceAdapter;
import com.verbosetech.weshare.chat.sendlocation.listners.AutoCompleteResults;
import com.verbosetech.weshare.chat.sendlocation.listners.LocationInfoListner;
import com.verbosetech.weshare.chat.sendlocation.listners.NearByPlacesResults;
import com.verbosetech.weshare.chat.sendlocation.listners.PlaceInfoListner;
import com.verbosetech.weshare.chat.sendlocation.models.GeocoderLocation;
import com.verbosetech.weshare.chat.sendlocation.models.NearByPlace;
import com.verbosetech.weshare.chat.sendlocation.models.PlaceModel;
import com.verbosetech.weshare.chat.sendlocation.utils.LocationManager;
import com.verbosetech.weshare.chat.sendlocation.utils.LocationUtils;
import com.verbosetech.weshare.util.Logger;
import com.verbosetech.weshare.view.MontserratMediumTextView;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
 * Created by developer on 4/1/19.
 */
public class SendLocationActivity extends AppCompatActivity {
    @BindView(R.id.chatToolbar)
    Toolbar chatToolbar;
    @BindView(R.id.tv_location)
    MontserratMediumTextView tvLocation;
    @BindView(R.id.rcv_locations)
    RecyclerView rcvLocations;
    @BindView(R.id.progress)
    ProgressBar progress;

    @BindView(R.id.tv_no_results)
    MontserratMediumTextView tvNoResults;

    private LocationManager locationManager;

    private GoogleMap mMap;
    String[] permissions = new String[]{
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
    };
    public static final int MULTIPLE_PERMISSIONS = 1111;
    //    private LatLng mCenterLatLong;
    private Location finalLocation;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_location);
        ButterKnife.bind(this);
        initUi();
        setUpMap();

    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        MenuItem searchItem = menu.findItem(R.id.actionSearch);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setQueryHint("Search..");
        initSearchView(searchView);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.actionSearch:
               /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    circleReveal(R.id.searchtoolbar, 1, true, true);
                else
                    searchtollbar.setVisibility(View.VISIBLE);

                item_search.expandActionView();*/
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initUi() {
        chatToolbar.setTitleTextAppearance(this, R.style.MontserratBoldTextAppearance);
        setSupportActionBar(chatToolbar);
        chatToolbar.setTitle("Send ChatLocation");

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_keyboard_arrow_left);
        }


    }

    public void initSearchView(final SearchView searchView) {

        // Enable/Disable Submit button in the keyboard
        searchView.setSubmitButtonEnabled(false);

        /*// Change search close button image
        ImageView closeButton = searchView.findViewById(R.id.search_close_btn);
        closeButton.setImageResource(R.drawable.ic_keyboard_arrow_right_black_24dp);*/


        // set hint and the text colors
        final EditText txtSearch = searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        txtSearch.setHint("Search...");
        txtSearch.setHintTextColor(Color.DKGRAY);
        txtSearch.setTextColor(getResources().getColor(R.color.white));


        // set the cursor
        AutoCompleteTextView searchTextView = searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, R.drawable.search_cursor); //This sets the cursor resource ID to 0 or @null which will make it visible on white background
        } catch (Exception e) {
            e.printStackTrace();
        }

        rcvLocations.setLayoutManager(new LinearLayoutManager(this));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                callSearch(query);
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //callSearch(newText);
                return true;
            }

        });

    }


    void callSearch(String query) {
        //Do searching
        Logger.getInstance().d("query------------->" + "" + query);
        progress.setVisibility(View.VISIBLE);
        LocationUtils.getAutoCompleteResults(SendLocationActivity.this,
                query,
                locationManager.getCurrentLocation(),
                new AutoCompleteResults() {
                    @Override
                    public void getListOfPlace(ArrayList<PlaceModel> modelArrayList) {
                        Logger.getInstance().d("modelArrayList-->" + modelArrayList);

                        setAdapter(modelArrayList);

                    }
                });

    }


    private void setUpMap() {
        locationManager = LocationManager.getInstance(this);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            MapsInitializer.initialize(this);
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    mMap = googleMap;
                    setCurrentLocationOnMap();
                }
            });
        }

    }

    private void setCurrentLocationOnMap() {
        if (checkPermissions()) {
            Logger.getInstance().e("----------->1" + checkPermissions());
            if (locationManager.isLocationOn()) {
                Logger.getInstance().e("----------->2" + locationManager.isLocationOn());
                pointMarkerOnMap(locationManager.getCurrentLocation());
            } else {
                Logger.getInstance().e("----------->3");
                locationManager.locationChecker();
            }
        } else {
            checkPermissions();
        }
    }

    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MULTIPLE_PERMISSIONS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (locationManager.isLocationOn()) {
                        callHandler();
                    } else {
                        locationManager.locationChecker();
                    }
                } else {
                    checkPermissions();
                    // no permissions granted.
                }
            }
        }
    }


    public void callHandler() {
        Handler handler = new Handler();
        if (locationManager.getCurrentLocation() != null) {
            callBookACabActivity();
        } else {
            handler.postDelayed(task, 500);
        }

    }

    private Runnable task = new Runnable() {
        @Override
        public void run() {
            if (locationManager.getCurrentLocation() != null) {
                callBookACabActivity();
            } else {
                callHandler();
            }

        }
    };


    public void callBookACabActivity() {
        Logger.getInstance().e("callBookACabActivity");
        pointMarkerOnMap(locationManager.getCurrentLocation());
    }

    private void pointMarkerOnMap(Location location) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                mMap.setMyLocationEnabled(true);
            }
        }

        // check if map is created successfully or not
        if (mMap != null) {
            // mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
            pointPinning(location);

        } else {
            Toast.makeText(this, "Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
        }

    }

    void pointPinning(Location location) {
        LatLng latLong = new LatLng(location.getLatitude(), location.getLongitude());

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLong).zoom(15f).tilt(10).build();
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));
        mMap.setOnCameraIdleListener(null);
        setLocation(location);

    }

    private void setLocation(final Location location) {
        if (location == null) {
            return;
        }
        LocationUtils.getLocationInfoByLatLong(this, location.getLatitude(),
                location.getLongitude(), new LocationInfoListner() {
                    @Override
                    public void requestsLocationInfo(GeocoderLocation locationrRes) {
                        finalLocation = new Location("pickUp");
                        finalLocation.setLatitude(location.getLatitude());
                        finalLocation.setLongitude(location.getLongitude());
                        finalLocation.setProvider(locationrRes.fullAdress);
                        Logger.getInstance().d("FulAddress-->" + locationrRes.fullAdress);
                        tvLocation.setText(locationrRes.fullAdress);

                        tvLocation.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Address address = new Address(Locale.getDefault());
                                address.setLatitude(location.getLatitude());
                                address.setLongitude(location.getLongitude());
                                address.setAddressLine(0, finalLocation.getProvider());
                                snapShotOfMap(address);
                            }
                        });

                    }
                });

        getNearByPlaces(location);
    }


    private void getNearByPlaces(Location location) {
        LocationUtils.getNearByPlaces(this, location, new NearByPlacesResults() {
            @Override
            public void getListOfNearByPlace(ArrayList<NearByPlace> nearByPlaceArrayList) {
                ArrayList<PlaceModel> placeModels = new ArrayList<>();
                for (int i = 0; i < nearByPlaceArrayList.size(); i++) {
                    NearByPlace byPlace = nearByPlaceArrayList.get(i);
                    if (byPlace != null) {
                        try {
                            addMarker(byPlace);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        PlaceModel placeModel = new PlaceModel();
                        placeModel.setPlaceid(byPlace.getPlace_id());
                        if (!TextUtils.isEmpty(byPlace.getVicinity())) {
                            placeModel.setDescription(byPlace.getVicinity());
                        } else {
                            placeModel.setDescription(byPlace.getReference());
                        }
                        placeModels.add(placeModel);
                    }
                }
                setAdapter(placeModels);
            }
        });
    }

    void addMarker(NearByPlace nearByPlace) {
        MarkerOptions markerOptions = new MarkerOptions();
        LatLng latLng = new LatLng(nearByPlace.getLatitude(), nearByPlace.getLongitude());
        markerOptions.position(latLng);
        //markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ring_blue));
        markerOptions.title(nearByPlace.getPlaceName() + " : " + nearByPlace.getVicinity());
        mMap.addMarker(markerOptions);


    }

    private void setAdapter(ArrayList<PlaceModel> modelArrayList) {
        progress.setVisibility(View.GONE);
        if (modelArrayList != null && modelArrayList.size() > 0) {
            rcvLocations.setVisibility(View.VISIBLE);
            tvNoResults.setVisibility(View.GONE);
            rcvLocations.setAdapter(new SearchPlaceAdapter(SendLocationActivity.this,
                    modelArrayList, new PlaceInfoListner() {
                @Override
                public void getPlaceInfo(Address address) {
                    if (address != null) {
                        snapShotOfMap(address);
                    }
                }
            }));
        } else {
            rcvLocations.setVisibility(View.GONE);
            tvNoResults.setVisibility(View.VISIBLE);
        }
    }

    private void snapShotOfMap(final Address address) {
        mMap.snapshot(new GoogleMap.SnapshotReadyCallback() {
            @Override
            public void onSnapshotReady(Bitmap bitmap) {


                String filePath = "/mnt/sdcard/"
                        + "COMMUNITY" + System.currentTimeMillis()
                        + ".png";
                try {
                    FileOutputStream fout = new FileOutputStream(filePath);
                    // Write the string to the file
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fout);
                    fout.flush();
                    fout.close();
                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    Log.d("ImageCapture", "FileNotFoundException");
                    Log.d("ImageCapture", e.getMessage());
                    filePath = "";
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    Log.d("ImageCapture", "IOException");
                    Log.d("ImageCapture", e.getMessage());
                    filePath = "";
                }


                // TODO Auto-generated method stub
                Intent intent = new Intent();
                intent.putExtra("location_data", address);
                intent.putExtra("map_snap", filePath);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }


}
