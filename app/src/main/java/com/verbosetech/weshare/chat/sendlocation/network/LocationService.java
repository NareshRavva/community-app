package com.verbosetech.weshare.chat.sendlocation.network;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/*
 * Created by developer on 4/1/19.
 */
public interface LocationService {

    @GET("/maps/api/geocode/json")
    Call<ResponseBody> getLocationInfo(@Query("sensor") boolean sensor,
                                       @Query("latlng") String location,
                                       @Query("key") String key);

    @GET("/maps/api/place/autocomplete/json")
    Call<ResponseBody> getAutoCompleteResults(@Query("sensor") boolean sensor,
                                              @Query("input") String input,
                                              @Query("location") String location,
                                              @Query("radius") Integer radius,
                                              @Query("key") String key);

    @GET("/maps/api/place/nearbysearch/json")
    Call<ResponseBody> getNearByPlaces(@Query("sensor") boolean sensor,
                                       @Query("location") String location,
                                       @Query("radius") Integer radius,
                                       @Query("key") String key);

    @GET("/maps/api/place/details/json")
    Call<ResponseBody> getPlaceInfo(@Query("placeid") String location,
                                    @Query("key") String key);
}
