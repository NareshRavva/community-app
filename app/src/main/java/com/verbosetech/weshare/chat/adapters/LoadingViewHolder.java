package com.verbosetech.weshare.chat.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.verbosetech.weshare.R;
import com.verbosetech.weshare.chat.models.ChatMessage;
import com.verbosetech.weshare.firestore.FireStoreOperations;
import com.verbosetech.weshare.util.Helper;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
 * Created by developer on 4/1/19.
 */
public class LoadingViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.card_view)
    CardView cardView;

    @BindView(R.id.progress)
    ProgressBar progress;

    LoadingViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        progress.setVisibility(View.VISIBLE);
    }

    public void setData(ChatMessage message, Context context) {
        boolean isMine = FireStoreOperations.getInstance().getUserId().equals(message.getSenderId());
        int _48dpInPx = Helper.dp2px(context, 48);
        cardView.setCardBackgroundColor(isMine ? ContextCompat.getColor(context, R.color.colorPrimary) : ContextCompat.getColor(context, R.color.colorPrimary_alpha));
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) cardView.getLayoutParams();
        if (isMine) {
            layoutParams.gravity = Gravity.END;
            layoutParams.setMargins(_48dpInPx, 0, 0, 0);
            //ll.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_chat_corner_right));
        } else {
            layoutParams.gravity = Gravity.START;
            layoutParams.setMargins(0, 0, _48dpInPx, 0);
            //ll.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_chat_corner_left));
        }
        cardView.setLayoutParams(layoutParams);
        progress.setVisibility(View.VISIBLE);
    }
}
