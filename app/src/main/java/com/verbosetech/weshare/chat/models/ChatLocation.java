package com.verbosetech.weshare.chat.models;

import lombok.Data;

/*
 * Created by developer on 6/1/19.
 */
@Data
public class ChatLocation {
    String name,mapSnapFilePath;
    double lat, lang;

}
