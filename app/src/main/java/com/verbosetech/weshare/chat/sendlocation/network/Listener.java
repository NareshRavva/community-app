package com.verbosetech.weshare.chat.sendlocation.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.Window;

import com.verbosetech.weshare.chat.sendlocation.listners.RetrofitService;
import com.verbosetech.weshare.util.Logger;

import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by RameshK on 25-11-2015.
 */
public class Listener implements Callback<ResponseBody> {

    Type tt;
    private RetrofitService listner;
    private ProgressDialog dialog;
    private Context activity;

    public Listener(RetrofitService listner,
                    String title,
                    boolean showProgress, Context activity) {
        //tt=t;
        this.listner = listner;
        dialog = new ProgressDialog(activity);

        if (title != null && title.length() > 0)
            dialog.setTitle(title);
        else
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        if (showProgress && dialog != null && !dialog.isShowing()) {
            dialog.show();
        }
        this.activity = activity;
    }

    public void showError(JSONObject obj) {
        if (!obj.isNull("message")) {

        }

    }


    @Override
    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (!response.isSuccessful()) {

                listner.onSuccess(response.message(), 2, null);
            } else {
                String res = response.body().string();
                Logger.getInstance().d("00000000000000000000000-->0" + res);
                listner.onSuccess(res, 0, null);
            }
        } catch (IOException | IllegalArgumentException e) {
            e.printStackTrace();
            Logger.getInstance().d("00000000000000000000000-->1" + e.getLocalizedMessage());
            listner.onSuccess("", 2, null);
        }
    }

    @Override
    public void onFailure(Call<ResponseBody> call, Throwable t) {
        Logger.getInstance().d("00000000000000000000000-->2" + t.getLocalizedMessage());
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        listner.onSuccess("", 1, t);
        if (t != null)
            t.printStackTrace();
    }
}
