package com.verbosetech.weshare.chat.sendlocation.utils;

/*
 * Created by developer on 5/1/19.
 */
public class LocationConstants {

    public static final String RESULTS = "results";
    public static final String STATUS = "status";

    public static final String OK = "OK";
    public static final String ZERO_RESULTS = "ZERO_RESULTS";
    public static final String REQUEST_DENIED = "REQUEST_DENIED";
    public static final String OVER_QUERY_LIMIT = "OVER_QUERY_LIMIT";
    public static final String UNKNOWN_ERROR = "UNKNOWN_ERROR";
    public static final String INVALID_REQUEST = "INVALID_REQUEST";

    //    Key for nearby places json from google
    public static final String GEOMETRY = "geometry";
    public static final String LOCATION = "location";
    public static final String LATITUDE = "lat";
    public static final String LONGITUDE = "lng";
    public static final String ICON = "icon";
    public static final String SUPERMARKET_ID = "id";
    public static final String NAME = "name";
    public static final String PLACE_ID = "place_id";
    public static final String REFERENCE = "reference";
    public static final String VICINITY = "vicinity";
    public static final String PLACE_NAME = "place_name";
    public static final String STATUS_KEY = "status";
    public static final String GEOMETRY_KEY = "geometry";
    public static final String RESULT_KEY = "result";
    public static final String LOCATION_KEY = "location";
    public static final String LATITUDE_KEY = "lat";
    public static final String LONGITUDE_KEY = "lng";
}
