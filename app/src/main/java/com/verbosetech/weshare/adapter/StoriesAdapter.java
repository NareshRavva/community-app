package com.verbosetech.weshare.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.verbosetech.weshare.R;
import com.verbosetech.weshare.feedCategories.FeedCategoryModel;
import com.verbosetech.weshare.fragment.HomeFeedsFragment;
import com.verbosetech.weshare.util.SharedPreferenceUtil;

import java.util.ArrayList;

/**
 * Created by laxmikant on 3/29/2018.
 */

public class StoriesAdapter extends RecyclerView.Adapter<StoriesAdapter.GroceryViewHolder> {
    private ArrayList<FeedCategoryModel> dataList;
    private Context context;
    private StoryClickListener storyClickListener;
    private SharedPreferenceUtil sharedPreferenceUtil;
    private String userMeImage;

    public StoriesAdapter(ArrayList<FeedCategoryModel> dataList, Context context, StoryClickListener storyClickListener) {
        this.dataList = dataList;
        this.context = context;
        this.storyClickListener = storyClickListener;
        sharedPreferenceUtil = new SharedPreferenceUtil(context);
        //FeedCategoryModel userResponse = Helper.getLoggedInUser(sharedPreferenceUtil);
        //userMeImage = userResponse != null ? userResponse.getImage() : "";
    }

    @Override
    public GroceryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GroceryViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_stories, parent, false));
    }

    @Override
    public void onBindViewHolder(GroceryViewHolder holder, int position) {
        holder.setData(dataList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class GroceryViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView, addIcon;
        private TextView txtview;
        private ProgressBar storyProgress;

        public GroceryViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.idProductImage);
            txtview = view.findViewById(R.id.idProductName);
            addIcon = view.findViewById(R.id.addIcon);
            storyProgress = view.findViewById(R.id.storyProgress);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    if (pos != -1) {
                        storyClickListener.showStory(pos);
//                        if (dataList.get(pos).getId() == -1) {
//                            storyClickListener.postStory();
//                        } else {
//                            storyClickListener.showStory(pos);
//                        }
                    }
                }
            });
        }

        public void setData(FeedCategoryModel feedCategoryModel, int position) {
//            Glide.with(context)
//                    .load(userResponse.getId() != -1 ? userResponse.getImage() : userMeImage)
//                    .apply(RequestOptions.bitmapTransform(new RoundedCorners(Helper.dp2px(context, 8))).placeholder(R.drawable.ic_person_gray_24dp))
//                    .into(imageView);
            txtview.setText(feedCategoryModel.getFeed_name());
            imageView.setImageResource(feedCategoryModel.getFeed_icon());

            if (HomeFeedsFragment.SELECTED_POS == position)
                imageView.setColorFilter(Color.parseColor("#ff0065"));
            else
                imageView.setColorFilter(Color.WHITE);

            //txtview.setSelected(true);
            //addIcon.setVisibility(userResponse.getId() != -1 ? View.GONE : View.VISIBLE);
            addIcon.setVisibility(View.GONE);
            storyProgress.setVisibility(View.GONE);
            //storyProgress.setVisibility(userResponse.getId() != -1 ? View.GONE : userResponse.isStoryUpdateProgress() ? View.VISIBLE : View.GONE);

//            if (userResponse.getId() != -1) {
//                imageView.setBackgroundResource(0);
//            } else {
//                imageView.setBackgroundColor(ContextCompat.getColor(context, R.color.trans_black_light));
//            }
        }
    }

    public interface StoryClickListener {
        void showStory(int pos);

        void postStory();
    }

}
