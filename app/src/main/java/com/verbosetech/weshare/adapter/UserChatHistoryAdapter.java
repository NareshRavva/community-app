package com.verbosetech.weshare.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.verbosetech.weshare.R;
import com.verbosetech.weshare.chat.activity.ChatActivity;
import com.verbosetech.weshare.listener.ContextualModeInteractor;
import com.verbosetech.weshare.listener.OnUserGroupItemClick;
import com.verbosetech.weshare.model.Post;
import com.verbosetech.weshare.model.UserData;
import com.verbosetech.weshare.model.UserMeta;
import com.verbosetech.weshare.util.Constants;
import com.verbosetech.weshare.util.Prefs;

import java.util.ArrayList;

public class UserChatHistoryAdapter extends RecyclerView.Adapter<UserChatHistoryAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<UserData> dataList;
    private OnUserGroupItemClick itemClickListener;
    private ContextualModeInteractor contextualModeInteractor;
    private int selectedCount = 0;

    public UserChatHistoryAdapter(Context context, ArrayList<UserData> dataList) {
        this.context = context;
        this.dataList = dataList;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.item_chat_user, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.setData(dataList.get(position));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView name, lastMessage, time;
        private ImageView image;
        private LinearLayout user_details_container;

        MyViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.user_name);
            time = itemView.findViewById(R.id.time);
            lastMessage = itemView.findViewById(R.id.message);
            image = itemView.findViewById(R.id.user_image);
            user_details_container = itemView.findViewById(R.id.user_details_container);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserData userData = dataList.get(getAdapterPosition());

                    Post post = new Post();
                    post.setCommunityId(Prefs.getString(Prefs.COMMUNITY_ID, ""));
                    UserMeta userMetaData = new UserMeta();
                    userMetaData.setId(userData.getId());
                    userMetaData.setName(userData.getName());
                    post.setUserMetaData(userMetaData);

                    Intent intent = new Intent(context, ChatActivity.class);
                    Gson gson = new Gson();
                    String postData = gson.toJson(post);
                    intent.putExtra(Constants.POST_DATA, postData);
                    context.startActivity(intent);
                }
            });

        }

        private void setData(UserData userData) {

            Glide.with(context).load(userData.getProfile_pic_tumbnail()).apply(new RequestOptions().placeholder(R.drawable.ic_person_gray_24dp)).into(image);

            name.setText(userData.getName());

            //time.setText(Helper.timeDiff(userData.getTimeUpdated()));
            //lastMessage.setText(chat.getLastMessage());
            //lastMessage.setTextColor(ContextCompat.getColor(context, !chat.isRead() ? R.color.colorText : R.color.colorTextSecondary));

            //user_details_container.setBackgroundColor(ContextCompat.getColor(context, (chat.isSelected() ? R.color.bg_gray : R.color.white)));
        }
    }


}
