package com.verbosetech.weshare.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.verbosetech.weshare.BuildConfig;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by a_man on 05-12-2017.
 */

public class ApiUtils {
    private static Retrofit retrofit;
    private static final String BASE_URL = "http://weshare-env-1.xf69i6mdqi.ap-south-1.elasticbeanstalk.com/";

    private ApiUtils() {
    }

    public static Retrofit getClient() {
        if (retrofit == null) {

            // Define the interceptor, add authentication headers
            Interceptor interceptor = new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request newRequest = chain.request().newBuilder().addHeader("Content-Type", "application/json").build();
                    return chain.proceed(newRequest);
                }
            };

            HttpLoggingInterceptor interceptorLog = new HttpLoggingInterceptor();
            if (BuildConfig.DEBUG) {
                interceptorLog.setLevel(HttpLoggingInterceptor.Level.BODY);
            } else {
                interceptorLog.setLevel(HttpLoggingInterceptor.Level.NONE);
            }

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            OkHttpClient defaultHttpClient = builder
                    .addInterceptor(interceptorLog)
                    .addInterceptor(interceptor)
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)
                    .build();

            Gson gson = new GsonBuilder().create();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(defaultHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }
}
