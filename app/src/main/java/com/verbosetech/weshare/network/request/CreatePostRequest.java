package com.verbosetech.weshare.network.request;

/**
 * Created by a_man on 05-12-2017.
 */

public class CreatePostRequest {
    private String title, text, type, media_url, video_thumbnail_url, category_id;
    private boolean is_story;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMedia_url() {
        return media_url;
    }

    public void setMedia_url(String media_url) {
        this.media_url = media_url;
    }

    public String getVideo_thumbnail_url() {
        return video_thumbnail_url;
    }

    public void setVideo_thumbnail_url(String video_thumbnail_url) {
        this.video_thumbnail_url = video_thumbnail_url;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public boolean isIs_story() {
        return is_story;
    }

    public CreatePostRequest(String title, String text, String type, String media_url, String category_id) {
        this.title = title;
        this.text = text;
        this.type = type;
        this.media_url = media_url;
        this.category_id = category_id;
    }

    public CreatePostRequest(String title, String text, String type, String category_id) {
        this.title = title;
        this.text = text;
        this.type = type;
        this.category_id = category_id;
    }

    public CreatePostRequest(String title, String text, String type, String media_url, String videoThumUrl, String category_id) {
        this.title = title;
        this.text = text;
        this.type = type;
        this.media_url = media_url;
        this.video_thumbnail_url = videoThumUrl;
        this.category_id = category_id;
    }

    public void setIs_story(boolean is_story) {
        this.is_story = is_story;
    }
}
