package com.verbosetech.weshare.model;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmModel;
import io.realm.annotations.RealmClass;

/**
 * Created by mayank on 11/5/17.
 */

@RealmClass
public class Attachment1 implements Parcelable, RealmModel {
    private String name, data, url;
    private long bytesCount;

    public Attachment1() {
    }

    protected Attachment1(Parcel in) {
        name = in.readString();
        data = in.readString();
        url = in.readString();
        bytesCount = in.readLong();
    }

    public static final Creator<Attachment1> CREATOR = new Creator<Attachment1>() {
        @Override
        public Attachment1 createFromParcel(Parcel in) {
            return new Attachment1(in);
        }

        @Override
        public Attachment1[] newArray(int size) {
            return new Attachment1[size];
        }
    };

    public String getName() {
        return name != null ? name : "";
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getBytesCount() {
        return bytesCount;
    }

    public void setBytesCount(long bytesCount) {
        this.bytesCount = bytesCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(data);
        dest.writeString(url);
        dest.writeLong(bytesCount);
    }
}

