package com.verbosetech.weshare.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class Comment {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("post_id")
    @Expose
    private String post_id;
    @SerializedName("user_profile_id")
    @Expose
    private UserMeta userMeta;
    @SerializedName("like_count")
    @Expose
    private int likeCount;
    @SerializedName("dislike_count")
    @Expose
    private int dislikeCount;
    @SerializedName("liked")
    @Expose
    private int liked;
    @SerializedName("disliked")
    @Expose
    private int disliked;
    @SerializedName("updated_at")
    @Expose
    private String updated_at;
    @SerializedName("created_at")
    @Expose
    private String created_at;
    @SerializedName("deleted_at")
    @Expose
    private String deleted_at;

    public String getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public String getPost_id() {
        return post_id;
    }

    public UserMeta getUserMeta() {
        return userMeta;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public int getDislikeCount() {
        return dislikeCount;
    }

    public int getLiked() {
        return liked;
    }

    public int getDisliked() {
        return disliked;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public void setDislikeCount(int dislikeCount) {
        this.dislikeCount = dislikeCount;
    }

    public void setLiked(int liked) {
        this.liked = liked;
    }

    public void setDisliked(int disliked) {
        this.disliked = disliked;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getDeleted_at() {
        return deleted_at;
    }
}
