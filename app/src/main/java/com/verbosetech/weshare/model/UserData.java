package com.verbosetech.weshare.model;

import lombok.Data;

/**
 * The type User data.
 */
/*
 * Created by developer on 30/1/18.
 */
@Data
public class UserData {

    private String
           id, name, email, os,
            profile_pic,
            profile_pic_tumbnail,
            device_token,
            online_status,
            mobile_no,
            registrationTime;


}
