package com.verbosetech.weshare.listener;

import com.verbosetech.weshare.model.Post;

import java.util.ArrayList;

/**
 * Created by a_man on 15-02-2018.
 */

public interface MainInteractor {
    ArrayList<Post> getBookmarkPosts();
}
