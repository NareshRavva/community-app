package com.verbosetech.weshare.listener;

import android.view.View;

import com.verbosetech.weshare.model.UserRealm;

public interface OnUserGroupItemClick {
    void OnUserClick(UserRealm user, int position, View userImage);
}
