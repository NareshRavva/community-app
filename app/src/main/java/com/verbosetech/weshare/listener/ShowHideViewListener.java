package com.verbosetech.weshare.listener;

public interface ShowHideViewListener {
    void showView();

    void hideView();
}
