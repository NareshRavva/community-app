package com.verbosetech.weshare.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.verbosetech.weshare.adapter.StoriesAdapter;
import com.verbosetech.weshare.feedCategories.FeedCategoryModel;
import com.verbosetech.weshare.listener.OnFragmentStateChangeListener;
import com.verbosetech.weshare.R;
import com.verbosetech.weshare.util.Constants;
import com.verbosetech.weshare.util.SpringAnimationHelper;

import java.util.ArrayList;

/**
 * A {@link Fragment} with options to post icon_text, icon_picture or video
 */
public class PostTypeFragment extends Fragment implements View.OnClickListener {

    private OnFragmentStateChangeListener onFragmentStateChangeListener;
    private StoriesAdapter storyAdapter;
    private ArrayList<FeedCategoryModel> storyUsers = new ArrayList<>();

    private RecyclerView recyclerStory;

    /**
     * returns new instance of {@link PostTypeFragment}
     *
     * @param onFragmentStateChangeListener A callback for different states of the {@link Fragment}
     * @return {@link PostTypeFragment}
     */
    public static PostTypeFragment newInstance(OnFragmentStateChangeListener onFragmentStateChangeListener) {
        PostTypeFragment postTypeFragment = new PostTypeFragment();
        postTypeFragment.onFragmentStateChangeListener = onFragmentStateChangeListener;
        return postTypeFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_add_text_pic, container, false);

        view.findViewById(R.id.frag_add_text_pic_txt).setOnClickListener(this);
        view.findViewById(R.id.frag_add_text_pic_pic).setOnClickListener(this);
        view.findViewById(R.id.frag_add_text_pic_video).setOnClickListener(this);

        recyclerStory = view.findViewById(R.id.recyclerStory);

        loadStoryUsers();

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        onFragmentStateChangeListener.onPause();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.frag_add_text_pic_empty_view:
                getActivity().getSupportFragmentManager().popBackStackImmediate();
                break;
            case R.id.frag_add_text_pic_video:
                SpringAnimationHelper.performAnimation(v);
                onFragmentStateChangeListener.onOther("video");
                break;
            case R.id.frag_add_text_pic_pic:
                SpringAnimationHelper.performAnimation(v);
                onFragmentStateChangeListener.onOther("image");
                break;
            case R.id.frag_add_text_pic_txt:
                SpringAnimationHelper.performAnimation(v);
                onFragmentStateChangeListener.onOther("text");
                break;
        }
    }

    private void loadStoryUsers() {
        storyAdapter = new StoriesAdapter(storyUsers, getContext(), new StoriesAdapter.StoryClickListener() {
            @Override
            public void showStory(int pos) {
                //startActivity(StatusActivity.newIntent(getContext(), storyUsers, pos));
                //SpringAnimationHelper.performAnimation(v);
                onFragmentStateChangeListener.onOther(storyUsers.get(pos).getFeed_name());
            }

            @Override
            public void postStory() {
                //if (!storyProgress)
                // pickMedia();
            }
        });
        recyclerStory.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recyclerStory.setAdapter(storyAdapter);

        getFeedCategoryList();

        storyUsers.clear();
        //storyUsers.add(new UserResponse(-1, "add", "add"));
        storyUsers.addAll(getFeedCategoryList());
        storyAdapter.notifyDataSetChanged();
    }

    private ArrayList<FeedCategoryModel> getFeedCategoryList() {

        ArrayList<FeedCategoryModel> arr_feed_types_data = new ArrayList<>();

        int images[] = new int[]{R.drawable.feed_all, R.drawable.feed_alert, R.drawable.feed_announcement, R.drawable.feed_ask,
                R.drawable.feed_events, R.drawable.feed_car_poll};

        String titles[] = new String[]{Constants.ALL_FEEDS, Constants.FEED_ALERT, Constants.FEED_ANNOUCE, Constants.FEED_ASK,
                Constants.FEED_EVENTS, Constants.FEED_CAR_POOL};

        for (int i = 0; i < images.length; i++) {
            FeedCategoryModel feed_model = new FeedCategoryModel();
            feed_model.setFeed_name(titles[i]);
            feed_model.setFeed_icon(images[i]);
            arr_feed_types_data.add(feed_model);
        }

        return arr_feed_types_data;
    }
}
