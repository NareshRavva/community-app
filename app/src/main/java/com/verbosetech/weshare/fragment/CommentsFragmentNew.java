package com.verbosetech.weshare.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.verbosetech.weshare.R;
import com.verbosetech.weshare.activity.MainActivity;
import com.verbosetech.weshare.adapter.CommentsRecyclerAdapter;
import com.verbosetech.weshare.chat.models.MessageModel;
import com.verbosetech.weshare.firestore.FireStoreConstants;
import com.verbosetech.weshare.firestore.FireStoreOperations;
import com.verbosetech.weshare.listener.OnCommentAddListener;
import com.verbosetech.weshare.listener.OnPopupMenuItemClickListener;
import com.verbosetech.weshare.model.Comment;
import com.verbosetech.weshare.model.UserMeta;
import com.verbosetech.weshare.network.DrService;
import com.verbosetech.weshare.util.Helper;
import com.verbosetech.weshare.util.Logger;
import com.verbosetech.weshare.util.Prefs;
import com.verbosetech.weshare.util.SpringAnimationHelper;
import com.verbosetech.weshare.view.MontserratEditText;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A screen to display and add the comments
 */
public class CommentsFragmentNew extends Fragment {

    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefreshs;
    @BindView(R.id.comment_recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.empty_view_container)
    LinearLayout emptyView;
    @BindView(R.id.empty_view_text)
    TextView emptyViewText;
    @BindView(R.id.commentContainer)
    LinearLayout commentContainer;
    @BindView(R.id.list_item_comment_foxy_img)
    ImageView profileIcon;
    @BindView(R.id.add_a_comment_edittext)
    MontserratEditText addACommentEdittext;
    @BindView(R.id.btn_post_comment)
    ImageView btnPostComment;

    private CommentsRecyclerAdapter commentsRecyclerAdapter;
    private String TAG = this.getClass().getSimpleName();
    private String postId;
    private ArrayList<Comment> commentArrayList;
    private static String previousPostId = "";
    private OnPopupMenuItemClickListener onPopupMenuItemClickListener;
    private OnCommentAddListener onCommentAddListener;
    private boolean isFullScreen = true;

    private int pageNumber = 1;

    private DrService foxyService;
    private boolean allDone, isLoading;
    private LinearLayoutManager linearLayoutManager;


   /* private void hideLoading() {
        isLoading = false;
        if (swipeRefresh.isRefreshing())
            swipeRefresh.setRefreshing(false);
        if (commentsRecyclerAdapter.isLoaderShowing())
            commentsRecyclerAdapter.hideLoading();
    }*/

    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            // init
            RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
            RecyclerView.Adapter adapter = recyclerView.getAdapter();

            if (layoutManager.getChildCount() > 0) {
                // Calculations..
                int indexOfLastItemViewVisible = layoutManager.getChildCount() - 1;
                View lastItemViewVisible = layoutManager.getChildAt(indexOfLastItemViewVisible);
                int adapterPosition = layoutManager.getPosition(lastItemViewVisible);
                boolean isLastItemVisible = (adapterPosition == adapter.getItemCount() - 1);
                // check
                if (isLastItemVisible && !isLoading && !allDone) {
                    pageNumber++;
                    commentsRecyclerAdapter.showLoading();
                    loadComments();
                }
            }
        }
    };

    private void loadComments() {
        isLoading = true;
        commentArrayList = new ArrayList<>();
        FirebaseFirestore.getInstance().
                collection(FireStoreConstants.TABLE_POST_COMMENTS).
                document(postId).
                collection(FireStoreConstants.COMMENTS)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.i(TAG, "listen:error" + e);
                            return;
                        }

                        for (DocumentChange dc : queryDocumentSnapshots.getDocumentChanges()) {
                            switch (dc.getType()) {
                                case ADDED:
                                    Logger.getInstance().i(TAG + "New city: " + dc.getDocument().getData() + "----" + dc.getDocument().toObject(MessageModel.class));
                                  /*  commentArrayList.add(dc.getDocument().toObject(Comment.class));
                                    commentsRecyclerAdapter.notifyDataSetChanged();
                                    linearLayoutManager.scrollToPosition(commentArrayList.size() - 1);*/

                                    commentsRecyclerAdapter.addItemOnTop(dc.getDocument().toObject(Comment.class));
                                    recyclerView.scrollToPosition(0);
                                    if (emptyView.getVisibility() == View.VISIBLE) {
                                        recyclerView.setVisibility(View.VISIBLE);
                                        emptyView.setVisibility(View.GONE);
                                    }
                                    break;
                                case MODIFIED:
                                    //showTyping(false);
                                    Logger.getInstance().i(TAG + "Modified city: " + dc.getDocument().getData());
                                    //updateItem(dc.getDocument().toObject(ChatMessage.class));
                                    break;
                                case REMOVED:
                                    Logger.getInstance().i(TAG + "Removed city: " + dc.getDocument().getData());
                                    break;
                            }

                        }
                    }
                });
    }

    /**
     * Returns new instance of {@link CommentsFragmentNew}
     *
     * @param postId                       The id of the post for which comments are to be shown and added
     * @param onPopupMenuItemClickListener A callback to denote the deletion or report of comment
     * @param onCommentAddListener         A callback which is used when new comment is added
     * @return {@link CommentsFragmentNew}
     */
    public static CommentsFragmentNew newInstance(String postId, OnPopupMenuItemClickListener onPopupMenuItemClickListener, OnCommentAddListener onCommentAddListener) {
        return newInstance(postId, true, onPopupMenuItemClickListener, onCommentAddListener);
    }


    /**
     * Returns new instance of {@link CommentsFragmentNew}
     *
     * @param postId                       The id of the post for which comments are to be shown and added
     * @param isFullScreen                 Denotes whether to open the fragment is fullscreen or not
     * @param onPopupMenuItemClickListener A callback to denote the deletion or report of comment
     * @param onCommentAddListener         A callback which is used when new comment is added
     * @return {@link CommentsFragmentNew}
     */
    public static CommentsFragmentNew newInstance(String postId, boolean isFullScreen, OnPopupMenuItemClickListener onPopupMenuItemClickListener, OnCommentAddListener onCommentAddListener) {
        CommentsFragmentNew commentsFragment = new CommentsFragmentNew();
        commentsFragment.postId = postId;
        commentsFragment.isFullScreen = isFullScreen;
        commentsFragment.onPopupMenuItemClickListener = onPopupMenuItemClickListener;
        commentsFragment.onCommentAddListener = onCommentAddListener;
        return commentsFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_comment, container, false);

        ButterKnife.bind(this, view);
        btnPostComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postCommentOnServer(v);
            }
        });

        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).hideBottomBar();
        }


//        if (!isFullScreen) {
//            recyclerView.setNestedScrollingEnabled(false);
//        }


       /* swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageNumber = 1;
                commentsRecyclerAdapter.clear();
                swipeRefresh.setRefreshing(true);
                loadComments();
            }
        });*/
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        commentsRecyclerAdapter = new CommentsRecyclerAdapter(getContext());
        recyclerView.setAdapter(commentsRecyclerAdapter);
        //recyclerView.addOnScrollListener(recyclerViewOnScrollListener);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        /*swipeRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeRefresh.setRefreshing(true);*/
        if (commentsRecyclerAdapter.isLoaderShowing())
            commentsRecyclerAdapter.hideLoading();
        loadComments();
    }

    /**
     * Handles the click of send button to post the comment.
     * It also validates the comment of not being empty
     *
     * @param view
     */
    public void postCommentOnServer(View view) {
        SpringAnimationHelper.performAnimation(view);

        final String commentTextToPost = addACommentEdittext.getText().toString();
        if (TextUtils.isEmpty(commentTextToPost)) {
            Toast.makeText(getContext(), "Comment can't be empty", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getContext(), "commented successfully", Toast.LENGTH_SHORT).show();
//            onCommentAddListener.onCommentAdded();
            updateComments(commentTextToPost);
            addACommentEdittext.setText("");
            Helper.closeKeyboard((Activity) getContext());
        }
    }

    private void updateComments(String commentTextToPost) {


        Comment comment = new Comment();

        comment.setId(System.currentTimeMillis() + "");
        comment.setText(commentTextToPost);
        comment.setPost_id(postId);
        //UsetData
        UserMeta userMeta = new UserMeta();
        userMeta.setId(FireStoreOperations.getInstance().getUserId());
        userMeta.setName(Prefs.getString(Prefs.USER_NAME, ""));

        comment.setUserMeta(userMeta);
        comment.setCreated_at(System.currentTimeMillis() + "");

        FirebaseFirestore.getInstance()
                .collection(FireStoreConstants.TABLE_POST_COMMENTS)
                .document(postId)
                .collection(FireStoreConstants.COMMENTS)
                .document()
                .set(comment);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        recyclerView.removeOnScrollListener(recyclerViewOnScrollListener);
        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).showBottomBar();
        }
    }

    /**
     * Requests the focus of the comment adding edit text
     */
    public void requestEditTextFocus() {
        addACommentEdittext.requestFocus();
    }
}
