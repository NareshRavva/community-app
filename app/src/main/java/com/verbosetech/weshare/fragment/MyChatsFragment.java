package com.verbosetech.weshare.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.verbosetech.weshare.R;
import com.verbosetech.weshare.adapter.UserChatHistoryAdapter;
import com.verbosetech.weshare.firestore.FireStoreConstants;
import com.verbosetech.weshare.firestore.FireStoreOperations;
import com.verbosetech.weshare.model.UserData;
import com.verbosetech.weshare.util.Logger;
import com.verbosetech.weshare.util.Prefs;
import com.verbosetech.weshare.view.MyRecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyChatsFragment extends Fragment {
    @BindView(R.id.recycler_view)
    MyRecyclerView recyclerView;
    private UserChatHistoryAdapter userChatHistoryAdapter;
    ArrayList<String> userIds = new ArrayList<>();
    ArrayList<UserData> userDataList = new ArrayList<>();
    private CollectionReference cr;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_recycler, container, false);

        ButterKnife.bind(this, view);

        Logger.getInstance().d("DR_EXYS->MyChatsFragment" + FireStoreOperations.getInstance().getUserId());
        cr = FirebaseFirestore.getInstance()
                .collection(FireStoreConstants.TABLE_USERS)
                .document(Prefs.getString(Prefs.COMMUNITY_ID, ""))
                .collection(FireStoreConstants.USERS_DATA);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView.setEmptyView(view.findViewById(R.id.emptyView));
        recyclerView.setEmptyImageView(((ImageView) view.findViewById(R.id.emptyImage)));
        recyclerView.setEmptyTextView(((TextView) view.findViewById(R.id.emptyText)));
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        userChatHistoryAdapter = new UserChatHistoryAdapter(getActivity(), userDataList);
        recyclerView.setAdapter(userChatHistoryAdapter);

        getChatUsersList();
    }


    private void getChatUsersList() {

        cr.document(FireStoreOperations.getInstance().getUserId()).
                collection(FireStoreConstants.USER_CHAT_HISTORY)
                .get().
                addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot documentSnapshots) {
                        if (documentSnapshots != null && !documentSnapshots.isEmpty()) {
                            for (DocumentSnapshot dc : documentSnapshots) {
                                if (dc.exists()) {
                                    Logger.getInstance().d("DR_EXYS------>X" + dc.getId());
                                    userIds.add(dc.getId());
                                }
                            }
                            getUsersData(0);
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Logger.getInstance().d("DR_EXYS------>X" + e.getLocalizedMessage());
            }
        });
    }

    private void getUsersData(final int indexGroup) {
        //if index equals list size setting data to adapter
        if (indexGroup == userIds.size()) {
            userChatHistoryAdapter.notifyDataSetChanged();
        } else {
            cr.document(userIds.get(indexGroup)).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    if (documentSnapshot != null && documentSnapshot.exists()) {
                        UserData userData = documentSnapshot.toObject(UserData.class);
                        if (userData != null) {
                            userData.setId(userIds.get(indexGroup));
                            userDataList.add(userData);
                        }
                    }
                    getUsersData(indexGroup + 1);
                }
            });
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


}
