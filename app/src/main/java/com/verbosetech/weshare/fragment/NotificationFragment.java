package com.verbosetech.weshare.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.verbosetech.weshare.adapter.NotificationRecyclerAdapter;
import com.verbosetech.weshare.model.Activity;
import com.verbosetech.weshare.R;
import com.verbosetech.weshare.network.ApiUtils;
import com.verbosetech.weshare.network.DrService;
import com.verbosetech.weshare.network.response.BaseListModel;
import com.verbosetech.weshare.util.Constants;
import com.verbosetech.weshare.util.Helper;
import com.verbosetech.weshare.util.SharedPreferenceUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Displays the notifications mimicking a popup
 */
public class NotificationFragment extends Fragment {
    RecyclerView recyclerView;
    LinearLayout emptyViewContainer;
    TextView empty_view_text;
    SwipeRefreshLayout swipeRefreshLayout;
    private NotificationRecyclerAdapter notificationAdapter;

    private DrService foxyService;
    private SharedPreferenceUtil sharedPreferenceUtil;
    private boolean allDone, isLoading;
    private int pageNumber = 1;
    private Call<BaseListModel<Activity>> getActivities;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferenceUtil = new SharedPreferenceUtil(getContext());
        foxyService = ApiUtils.getClient().create(DrService.class);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);

        recyclerView = view.findViewById(R.id.fragment_notification_recycler_view);
        emptyViewContainer = view.findViewById(R.id.empty_view_container);
        empty_view_text = view.findViewById(R.id.empty_view_text);
        swipeRefreshLayout = view.findViewById(R.id.frag_home_feeds_swipe_refresh_layout);

        ViewCompat.setElevation(view, Helper.dpToPx(getContext(), 8));
        view.bringToFront();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageNumber = 1;
                notificationAdapter.clear();
                allDone = false;
                loadPosts();
                recyclerView.setVisibility(View.VISIBLE);
                emptyViewContainer.setVisibility(View.GONE);
                //addInitialPosts(true);
            }
        });

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addOnScrollListener(recyclerViewOnScrollListener);
        notificationAdapter = new NotificationRecyclerAdapter(getContext());
        recyclerView.setAdapter(notificationAdapter);

        initialize();

        return view;
    }

    private void initialize() {
        loadPosts();
        recyclerView.setVisibility(View.VISIBLE);
        emptyViewContainer.setVisibility(View.GONE);
    }

    private void loadPosts() {
        isLoading = true;
        getActivities = foxyService.getActivities(sharedPreferenceUtil.getStringPreference(Constants.KEY_API_KEY, null), pageNumber);
        getActivities.enqueue(callBack);
    }

    private Callback<BaseListModel<Activity>> callBack = new Callback<BaseListModel<Activity>>() {
        @Override
        public void onResponse(Call<BaseListModel<Activity>> call, Response<BaseListModel<Activity>> response) {
            isLoading = false;
            if (notificationAdapter.isLoaderShowing())
                notificationAdapter.hideLoading();
            if (swipeRefreshLayout.isRefreshing())
                swipeRefreshLayout.setRefreshing(false);
            if (response.isSuccessful()) {
                BaseListModel<Activity> postResponse = response.body();
                if (postResponse.getData() == null || postResponse.getData().isEmpty()) {
                    allDone = true;
                    if (notificationAdapter.itemsList.isEmpty()) {
                        recyclerView.setVisibility(View.GONE);
                        emptyViewContainer.setVisibility(View.VISIBLE);
                        empty_view_text.setText("Nothing found..");
                    }
                } else {
                    notificationAdapter.addItemsAtBottom(postResponse.getData());
                }
            } else {
                recyclerView.setVisibility(View.GONE);
                emptyViewContainer.setVisibility(View.VISIBLE);
                empty_view_text.setText("Nothing found..");
            }
        }

        @Override
        public void onFailure(Call<BaseListModel<Activity>> call, Throwable t) {
            isLoading = false;
            if (notificationAdapter.isLoaderShowing())
                notificationAdapter.hideLoading();
            if (swipeRefreshLayout.isRefreshing())
                swipeRefreshLayout.setRefreshing(false);
            recyclerView.setVisibility(View.GONE);
            emptyViewContainer.setVisibility(View.VISIBLE);
            empty_view_text.setText("Something went wrong");
        }
    };

    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            // init
            RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
            RecyclerView.Adapter adapter = recyclerView.getAdapter();

            if (layoutManager.getChildCount() > 0) {
                // Calculations..
                int indexOfLastItemViewVisible = layoutManager.getChildCount() - 1;
                View lastItemViewVisible = layoutManager.getChildAt(indexOfLastItemViewVisible);
                int adapterPosition = layoutManager.getPosition(lastItemViewVisible);
                boolean isLastItemVisible = (adapterPosition == adapter.getItemCount() - 1);
                // check
                if (isLastItemVisible && !isLoading && !allDone) {
                    pageNumber++;
                    notificationAdapter.showLoading();
                    loadPosts();
                }
            }
        }
    };
}
