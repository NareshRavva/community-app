package com.verbosetech.weshare.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.verbosetech.weshare.R;
import com.verbosetech.weshare.listener.MainInteractor;
import com.verbosetech.weshare.util.Helper;
import com.verbosetech.weshare.util.SharedPreferenceUtil;

/**
 * Created by a_man on 26-12-2017.
 */

public class BookmarksFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bookmark, container, false);
        inflatePosts();
        return view;
    }

    private void inflatePosts() {
        getChildFragmentManager()
                .beginTransaction()
                .replace(R.id.frameBookmark, HomeFeedsFragment.newInstance("bookmark", Helper.getBookmarkedPosts(new SharedPreferenceUtil(getContext())), -1, false, null), "bookmark")
                .commit();
    }
}
