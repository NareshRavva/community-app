package com.verbosetech.weshare.communityList;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.verbosetech.weshare.R;
import com.verbosetech.weshare.firestore.FireStoreConstants;
import com.verbosetech.weshare.util.Constants;
import com.verbosetech.weshare.util.Logger;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommunityListActivity extends AppCompatActivity {

    @BindView(R.id.rcvCommList)
    RecyclerView rcvCommList;
    @BindView(R.id.commProgress)
    ProgressBar commProgress;

    private ArrayList<CommunityListModel> modelArrayList = new ArrayList<>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_community_list);
        ButterKnife.bind(this);

        getCommunityList();
       /* LinearLayout llLayout = findViewById(R.id.llLayout);

        llLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent main = new Intent(CommunityListActivity.this, SubmitProofActivity.class);
                startActivity(main);
                finish();
            }
        });*/
    }

    private void getCommunityList() {

        commProgress.setVisibility(View.VISIBLE);
        rcvCommList.setVisibility(View.GONE);
        CollectionReference cr = FirebaseFirestore.getInstance()
                .collection(FireStoreConstants.TABLE_COMMUNITY_LIST);
        Logger.getInstance().d("CommunityListModel-->" + cr);
        cr.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                commProgress.setVisibility(View.GONE);
                rcvCommList.setVisibility(View.VISIBLE);
                for (QueryDocumentSnapshot dc : task.getResult()) {
                    modelArrayList.add(dc.toObject(CommunityListModel.class));
                }
                Logger.getInstance().d("CommunityListModel-->" + modelArrayList);
                rcvCommList.setLayoutManager(new LinearLayoutManager(CommunityListActivity.this));
                rcvCommList.setAdapter(new CommunityListAdapter(CommunityListActivity.this, modelArrayList, new CommunityItemClickListner() {
                    @Override
                    public void getCommunity(CommunityListModel communityListModel) {
                        Gson gson = new Gson();
                        String comModel = gson.toJson(communityListModel);
                        Intent main = new Intent(CommunityListActivity.this,
                                SubmitProofActivity.class);
                        main.putExtra(Constants.COMMUNITY_DATA, comModel);
                        startActivity(main);
                        finish();
                    }
                }));
            }
        });

    }
}
