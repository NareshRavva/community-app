package com.verbosetech.weshare.communityList;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.gson.Gson;
import com.verbosetech.weshare.R;
import com.verbosetech.weshare.activity.MainActivity;
import com.verbosetech.weshare.activity.UpdateProfileActivity;
import com.verbosetech.weshare.util.Constants;
import com.verbosetech.weshare.util.Logger;
import com.verbosetech.weshare.view.MontserratBoldTextView;
import com.verbosetech.weshare.view.MontserratMediumTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubmitProofActivity extends AppCompatActivity {
    @BindView(R.id.tv_skip)
    MontserratBoldTextView tvSkip;

    @BindView(R.id.tv_name_location)
    MontserratBoldTextView tvNameLocation;
    @BindView(R.id.tv_members)
    MontserratMediumTextView tvMembers;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_proof);
        ButterKnife.bind(this);
        final String commModel = getIntent().getStringExtra(Constants.COMMUNITY_DATA);
        setDataToViews(commModel);
        Logger.getInstance().d("CommModel-->" + commModel);


        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent main = new Intent(SubmitProofActivity.this,
                        UpdateProfileActivity.class);
                main.putExtra(Constants.COMMUNITY_DATA, commModel);
                startActivity(main);
                finish();

            }
        });

    }

    private void setDataToViews(String commModel) {
        Gson gson = new Gson();
        CommunityListModel model = gson.fromJson(commModel, CommunityListModel.class);
        if (model != null) {
            tvNameLocation.setText(model.getDisplay_name() + " , " + model.getLocation());
        }
    }
}
