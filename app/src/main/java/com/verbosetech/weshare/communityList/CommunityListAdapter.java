package com.verbosetech.weshare.communityList;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.verbosetech.weshare.R;
import com.verbosetech.weshare.firestore.FireStoreConstants;
import com.verbosetech.weshare.firestore.FireStoreOperations;
import com.verbosetech.weshare.util.Helper;
import com.verbosetech.weshare.util.Prefs;
import com.verbosetech.weshare.view.MontserratMediumTextView;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
 * Created by developer on 9/1/19.
 */
public class CommunityListAdapter extends RecyclerView.Adapter<CommunityListAdapter.CommunityView> {

    private final CommunityItemClickListner listner;
    private Context context;
    private ArrayList<CommunityListModel> modelArrayList;

    public CommunityListAdapter(Context context,
                                ArrayList<CommunityListModel> communityListModels,
                                CommunityItemClickListner listner) {
        this.context = context;
        this.modelArrayList = communityListModels;
        this.listner = listner;
    }

    @NonNull
    @Override
    public CommunityView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        return new CommunityView(layoutInflater.inflate(R.layout.community_view, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CommunityView holder, int position) {
        final CommunityListModel communityListModel = modelArrayList.get(position);
        if (communityListModel != null) {
            holder.tvCommName.setText(modelArrayList.get(position).getDisplay_name());
            holder.tvLocation.setText(modelArrayList.get(position).getLocation());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    createUserInCommunityList(communityListModel);


                }
            });
        }
    }

    private void createUserInCommunityList(final CommunityListModel communityListModel) {
        final ProgressDialog pdg = Helper.getProgressDialog(context);
        HashMap<String, String> commIds = new HashMap<>();
        commIds.put(FireStoreConstants.COMMUNITY_ID, communityListModel.getId());
        FirebaseFirestore.getInstance().
                collection(FireStoreConstants.TABLE_COMMUNITY_USER).
                document(FireStoreOperations.getInstance().getUserId()).
                set(commIds, SetOptions.merge()).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                pdg.dismiss();
                listner.getCommunity(communityListModel);
                Prefs.putString(Prefs.COMMUNITY_ID,communityListModel.getId());

            }
        });

    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class CommunityView extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_comm_name)
        MontserratMediumTextView tvCommName;
        @BindView(R.id.tv_location)
        MontserratMediumTextView tvLocation;

        public CommunityView(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
