package com.verbosetech.weshare.communityList;

import lombok.Data;

@Data
public class CommunityListModel {

    String display_name;
    String id;
    String about;
    String location;
}
