package com.verbosetech.weshare.feedCategories;

public class FeedCategoryModel {

    String feed_name;
    int feed_icon;

    public String getFeed_name() {
        return feed_name;
    }

    public void setFeed_name(String feed_name) {
        this.feed_name = feed_name;
    }

    public int getFeed_icon() {
        return feed_icon;
    }

    public void setFeed_icon(int feed_icon) {
        this.feed_icon = feed_icon;
    }
}
